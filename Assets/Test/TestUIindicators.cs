﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestUIindicators : MonoBehaviour
{

    [SerializeField]
    private Text enemyCountText;

    [SerializeField]
    private Text bonusCountText;

    [SerializeField]
    private Text nuetralCountText;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        enemyCountText.text = SceneObjectsManager.Instance.Enemies.Count.ToString();
        bonusCountText.text = SceneObjectsManager.Instance.Bonuses.Count.ToString();
        nuetralCountText.text = SceneObjectsManager.Instance.Substances.Count.ToString();
    }
}
