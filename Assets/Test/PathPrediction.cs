﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PathPrediction : MonoBehaviour {

    public int MaxReflectionCount = 5;
    public float MaxStepDistance = 200;
    //for input data
    private Vector3 initialDirection = new Vector3(600, 600, 0);

	void Update ()
    {

    }

    private void OnDrawGizmos()
    {
        DrawPredictedReflection(transform.position, initialDirection, MaxReflectionCount);
    }

    private void DrawPredictedReflection(Vector3 position, Vector3 direction, int reflectionsRemaining)
    {
        if (reflectionsRemaining == 0)
        {
            return;
        }

        Vector3 startingPosition = position;

        Ray ray = new Ray(position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, MaxStepDistance))
        {
            direction = Vector3.Reflect(direction, hit.normal);
            position = hit.point;
        }
        else
        {
            position += direction * MaxStepDistance;
        }

        Debug.DrawLine(startingPosition, position, Color.yellow);

        DrawPredictedReflection(position, direction, reflectionsRemaining - 1);
    }
}
