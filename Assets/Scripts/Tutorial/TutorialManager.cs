﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct TutorialTargetData
{
    public int Number;
    public TutorialTarget Target;
    public GameObject Object;
    public string Text;
}

public class TutorialImagesManager : MonoBehaviour
{
    public static TutorialImagesManager Instance { get; private set; }

    //public bool IsComplete = false;
    public bool IsActivated;

    private Action<bool> StopTutorialAction; 

    [SerializeField]
    private TutorialTargetData[] m_TutorialSteps;

    [SerializeField]
    private GameObject[] m_TutorialsObjects;

    private List<ITutorialElement> elements = new List<ITutorialElement>();

    private ITutorialElement currentTutorial;
    int currentNumber = 0;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void StartTutorial()
    {
        if (elements.Count == 0)
        {
            foreach (var item in m_TutorialsObjects)
            {
                elements.Add(item.GetComponent<ITutorialElement>());
            }
        }

        currentTutorial = null;
        //TODO сделать проверку на максимальный номер в m_TutorialSteps
        while (currentNumber <= m_TutorialSteps.Length)
        {
            foreach (var elm in elements)
            {
                if(currentNumber == elm.Number)
                {
                    currentTutorial = elm;
                    break;
                }
            }
            
            if(currentTutorial != null)
            {
                foreach (var data in m_TutorialSteps)
                {
                    if(data.Number == currentNumber)
                    {
                        currentTutorial.StopAction += OnTutorialStoped;
                        currentTutorial.StartTutorial(data.Object, data.Target);
                        IsActivated = true;
                        return;
                    }
                }   
            }

            currentNumber++;
        }
        Destroy(gameObject);
    }

    private void OnTutorialStoped(bool isBreak)
    {
        currentTutorial.StopAction -= OnTutorialStoped;
        currentNumber++;
        IsActivated = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && currentNumber == 0)
        {
            StartTutorial();
        }
    }

}
