﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuturialEnemyWave : MonoBehaviour {

    private List<GameObject> _children = new List<GameObject>();
    public List<GameObject> Children
    {
        get{
                return _children;
            }
        private set
        {
            _children = value;
        }
    }

    private void Awake()
    {
        var length = transform.childCount;
        for (int i = 0; i < length; i++)
        {
            Children.Add(transform.GetChild(i).gameObject);
        }
    }
}
