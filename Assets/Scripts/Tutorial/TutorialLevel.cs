﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TutorialLevel : MonoBehaviour
{
    private int tutorialBalls = 3;
    private float betweenBallsTimer = 0.1f;
    [SerializeField]
    private TuturialEnemyWave[] Waves;
    [SerializeField]
    private Animator Canvas;
    [SerializeField]
    private Animator Enemy;
    [SerializeField]
    private LineRenderer Player;
    [SerializeField]
    private GameObject AIM;
    [SerializeField]
    private Button m_Button;
    [SerializeField]
    private TutorialBallShooting m_ShootingTutorial;
    [SerializeField]
    private Animator m_tutorialPanel;

    private List<GameObject> enemies = new List<GameObject>();

    public static bool isPlaying
    {
        get
        {
            return Instance != null;
        }
    }

    public static TutorialLevel Instance
    {
        get;
        private set;
    }

    public bool next = false;

    private void Awake()
    {
        Instance = this;
        foreach (var item in Waves[0].Children)
        {
            enemies.Add(item);
        }
        foreach (var item in Waves[1].Children)
        {
            enemies.Add(item);
        }
    }

    private void Start()
    {
        Invoke("OpenTutorPanel", 0.4f);
    }

    public void OnClickNext()
    {
        next = true;
        m_Button.enabled = false;
        m_Button.gameObject.SetActive(false);
    }

    public void LoadMainMenu()
    {
        SceneLoader.LoadLevel(new NextLevelData("MainMenu", true));
    }

    public void StartTutorial()
    {
        StartCoroutine(StartTutorialCoroutine());
        CustomAnalytics.Instance.StartTutorial();
    }

    private IEnumerator StartTutorialCoroutine()
    {
        InputManager.Instance.IsTutorialMode = true;
        Canvas.SetTrigger("next");
        var anim = Waves[0].Children.Single(x => x.activeSelf).GetComponent<Animator>();
        yield return new WaitForSeconds(0.5f);
        anim.SetBool("move", true);
        yield return new WaitForSeconds(1f);
        m_Button.enabled = true;
        //ждем подтверждения пользователя на шаге 1
        while (!next)
        {
            yield return null;
        }
        anim.SetBool("move", false);
        yield return new WaitForSeconds(0.2f);
        Canvas.SetTrigger("next");
        //ждем подтверждения пользователя на шаге 2
        yield return new WaitForSeconds(1f);
        next = false;
        m_Button.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        m_Button.enabled = true;
        while (!next)
        {
            yield return null;
        }
        Canvas.SetTrigger("next");
        Enemy.SetTrigger("next");
        yield return new WaitForSeconds(7f);
        HealthUI.Instance.SetPlayerHP(2, 5, -3);
        next = false;
        m_Button.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        m_Button.enabled = true;
        while (!next)
        {
            yield return null;
        }
        Canvas.SetTrigger("next");
        yield return new WaitForSeconds(0.8f);

        foreach (var item in Waves[0].Children)
        {
            if (item)
            {
                item.SetActive(true);
                Vector3 pos = item.transform.position;
                pos.z = 11f;
                item.transform.position = pos;
            }
        }

        next = false;
        GameStateMachine.Instance.ChangeState(GameStates.PlayerTurn);
        IEnumerator shootCoroutine = m_ShootingTutorial.StartTutorial();
        while (!next)
        {
            yield return null;
        }

        //Enemy.SetTrigger("next");
        //yield return new WaitForSeconds(5.5f);

        //Enemy.SetTrigger("next");
        //var pos1 = AIM.transform.position;
        //var pos2 = Player.transform.position;
        //Vector3[] list = new Vector3[] { pos1,pos2 };
        //Player.enabled = true;
        //Player.positionCount = 2;
        //var time = Time.time;
        //while (true)
        //{
        //    pos1 = AIM.transform.position;
        //    pos2 = Player.transform.position;
        //    list = new Vector3[] { pos1, pos2 };
        //    Player.SetPositions(list);
        //    yield return new WaitForEndOfFrame();
        //    if (Time.time - time > 2.5f)
        //    {
        //        Player.gameObject.GetComponent<PlayerController>().PushBall();
        //        break;
        //    }
        //}
        //yield return new WaitForSeconds(3.5f);
        //foreach (var item in Waves[0].Children)
        //{
        //    if (item)
        //    {
        //        item.SetActive(true);
        //    }
        //}

        yield return new WaitForSeconds(1.2f);
        Waves[1].gameObject.SetActive(true);
        foreach (var item in Waves[1].Children)
        {
            if (item)
            {
                item.SetActive(true);
                Vector3 pos = item.transform.position;
                pos.z = 11f;
                item.transform.position = pos;
            }
        }
        Canvas.SetTrigger("next");
        next = false;
        m_Button.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        m_Button.enabled = true;
        while (!next)
        {
            yield return null;
        }
        
        //foreach (var ball in BallsManager.Instance.ActiveBalls)
        //{
        //    ball.SetActive(false);
        //}
        BallsManager.Instance.ActiveBalls = new List<GameObject>();
        //GameStateMachine.Instance.ChangeState(GameStates.EnemyTurn);

        Canvas.SetTrigger("next");
        //next = false;
        //m_Button.gameObject.SetActive(true);
        //yield return new WaitForSeconds(1f);
        //m_Button.enabled = true;
        InputManager.Instance.IsTutorialMode = false;
        HealthUI.Instance.SetPlayerHP(5, 5, 0);
        //LevelRoundLoader.Instance.levelNumber = 1;
        //var ball = FindObjectOfType<BallBehavior>();
        //if (ball)
        //{
        //    Destroy(ball.gameObject);
        //}
        //while (!next)
        //{
        //    print("5");
        //    yield return new WaitForEndOfFrame();
        //}

        while (true)
        {
            EnemyMovement enemy = FindObjectOfType<EnemyMovement>();
            if (!enemy)
            {
                yield return new WaitForSeconds(2f);
                break;
            }
            yield return null;
        }

        if (SceneManagerScript.Instance.player.GetComponent<PlayerHealth>().HP > 0)
        {
            DlgWndController.instace.Tutorial();
        }



    }
    private void OnDestroy()
    {
        Instance = null;
    }
    public static void OnLevelBuilt()
    {
        if (isPlaying)
        {
            if (SceneManagerScript.Instance.OnLevelBuilt != null)
            {
                SceneManagerScript.Instance.OnLevelBuilt.Invoke();
            }
            else
            {
                print("SceneManagerScript.OnLevelBuilt = null");
            }
        }
    }
    public static int BallsCount(int countBalls)
    {
        if (isPlaying)
        {
            return Instance.tutorialBalls;
        }
        return countBalls;
    }
    public static float BetweenBallsTimer(float betweenBallsTimer)
    {
        if (isPlaying)
        {
            return Instance.betweenBallsTimer;
        }
        return betweenBallsTimer;
    }
    public static SceneObjectsData SceneObjectsData(SceneObjectsData data)
    {
        if (isPlaying)
        {
            return Resources.Load<SceneObjectsData>("ObjectsDatabases/TutorialObjectData");
        }
        return data;
    }
    public static void BuildNextRound()
    {
        if (isPlaying)
        {
            WavePanel.Instance.SetRoundCount(18);
            var res = true;
            foreach (var item in Instance.Waves[1].Children)
            {
                if (item)
                {
                    res = false;
                    break;
                }
            }
            Instance.next = res;
        }
    }

    private void OpenTutorPanel()
    {
        m_tutorialPanel.SetTrigger("Open");
    }
}
