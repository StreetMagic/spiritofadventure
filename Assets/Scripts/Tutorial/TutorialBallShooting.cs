﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialBallShooting : MonoBehaviour, ITutorialElement
{
    [SerializeField]
    private Image m_background;

    [SerializeField]
    private GameObject m_finger;

    [SerializeField]
    private GameObject m_fingerActive;

    [SerializeField]
    private GameObject m_circle;

    [SerializeField]
    private GameObject m_button;

    [SerializeField]
    private TutorialLevel m_tutorialSteps;

    [SerializeField]
    private PlayerController m_player;
    private object obj;
    private TutorialTarget target;

    private IEnumerator myCoroutine;
    private Animator myAnim;

    private InputData currentData;
    private float timer = 3f;

    private Vector3 circlePos;
    private Vector3 circleScale;
    private Vector3 fingerPos;


    public int Number
    {
        get
        {
            return 1;
        }
    }

    public Action<bool> StopAction { get; set; }

    public void StartTutorial(object obj, TutorialTarget target)
    {
        if (myCoroutine != null)
            return;
        myCoroutine = TutorialCoroutine();
        StartCoroutine(myCoroutine);
    }

    public IEnumerator StartTutorial()
    {
        if (myCoroutine != null)
            return null;
        myCoroutine = TutorialCoroutine();
        StartCoroutine(myCoroutine);
        return myCoroutine;
    }

    public void StopTutorial(bool isBreak)
    {
        if (StopAction != null)
            StopAction(true);
        StopTutorial();
        m_button.SetActive(false);
    }

    public void FinishTutorial()
    {
        StopTutorial(true);
        m_tutorialSteps.next = true;
    }

    private void StopTutorial()
    {
        m_finger.SetActive(false);
        m_circle.SetActive(false);
        //m_background.gameObject.SetActive(false);
        ResetTutorial();
        InputManager.Instance.IsTutorialMode = false;
        StopCoroutine(myCoroutine);
        myCoroutine = null;
    }

    // Use this for initialization
    void Start()
    {
        myAnim = m_background.transform.parent.gameObject.GetComponent<Animator>();
        circlePos = m_circle.transform.position;
        circleScale = m_circle.transform.localScale;
        fingerPos = m_finger.transform.position;
    }

    private IEnumerator TutorialCoroutine()
    {
        timer = 3f;
        InputManager.Instance.IsTutorialMode = true;
        //m_button.SetActive(true);
        m_background.gameObject.SetActive(true);
        while (m_background.color.a < 0.55f)
        {
            Color color = m_background.color;
            color.a += 0.01f;
            m_background.color = color;
            yield return null;
        }
        //yield return new WaitForSeconds(0.3f);
        //m_circle.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        m_finger.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        myAnim.SetTrigger("Next");
        yield return new WaitForSeconds(2f);
        m_finger.GetComponent<Image>().enabled = false;
        m_fingerActive.GetComponent<Image>().enabled = true;
        myAnim.SetTrigger("Next");
        m_player.BallsAtOnce = 3;
        while (timer > 0)
        {
            currentData = new InputData();
            currentData.CurrentPosition = m_circle.transform.position;
            InputManager.Instance.LongPressAction(currentData);
            timer -= Time.deltaTime;
            yield return null;
        }
        InputManager.Instance.TapAction(currentData);
        m_finger.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        m_circle.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        while (m_background.color.a > 0)
        {
            Color color = m_background.color;
            color.a -= 0.01f;
            m_background.color = color;
            yield return null;
        }
        m_background.gameObject.SetActive(false);
        myCoroutine = null;
        ResetTutorial();
        m_tutorialSteps.next = true;
        //yield return new WaitForSeconds(0.7f);
        //while (BallsManager.ActiveBalls.Count >0)
        //{
        //    yield return null;
        //}
        //StartTutorial();
    }

    private void ResetTutorial()
    {
        myAnim.SetTrigger("Restart");
        m_circle.transform.position = circlePos;
        m_circle.transform.localScale = circleScale;
        m_finger.transform.position = fingerPos;
        m_finger.GetComponent<Image>().enabled = true;
        m_fingerActive.GetComponent<Image>().enabled = false;
    }
}
