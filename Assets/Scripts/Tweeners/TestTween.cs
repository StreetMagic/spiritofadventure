﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TestTween : MonoBehaviour
{
    Action ObjectStopped;
    public Transform target;
    public AnimationCurve AnimationCurve;


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            StartTwwen();
        }
    }

    // Use this for initialization
    void StartTwwen ()
    {
        ObjectStopped += OnObjectStopped;
        Tweener.TweenToPosition(transform, transform.position, target.position, 3f, ObjectStopped, AnimationCurve);
        //Invoke("Change", 2f);
	}

    private void Change()
    {
        Tweener.TweenToPosition(transform, transform.position, new Vector3(12, 12, 12), 3, ObjectStopped);
    }

    private void OnObjectStopped()
    {
        ObjectStopped -= OnObjectStopped;
        print("object stopped");
    }
}
