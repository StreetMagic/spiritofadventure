﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SubstanceBehavior : MonoBehaviour, INeutral
{
    protected bool isReady = false;

    public abstract bool ReadyCheck();
    public abstract void RegisterSubstance();
}
