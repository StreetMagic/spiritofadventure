﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplosion : MonoBehaviour
{
    private int expDamage;
    private int expRadius;

    private ObjectHealth myHealth;

    // Use this for initialization
    void Start()
    {
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        expDamage = data.GetObjectByType(ObjectType.bomb).Object_ExplosionDamage;
        expRadius = data.GetObjectByType(ObjectType.bomb).Object_ExplosionRadius;
        myHealth = GetComponent<ObjectHealth>();
        myHealth.OnDeathAction += OnDeathExplosion;
    }

    private void OnDestroy()
    {
        myHealth.OnDeathAction -= OnDeathExplosion;
    }

    [ContextMenu("Debug Explosion")]
    public void Split()
    {
        OnDeathExplosion();
    }

    private void OnDeathExplosion()
    {
        Collider[] enemies = Physics.OverlapSphere(transform.position, expRadius);

        if (enemies.Length <= 0)
            return;

        foreach (Collider item in enemies)
        {
            if (item.gameObject == gameObject)
                continue;
            ObjectHealth health = item.GetComponent<ObjectHealth>();
            if (health)
            {
                if (health.HP > 0)
                    health.TakeDamage(gameObject, expDamage);
            }
        }
    }
}
