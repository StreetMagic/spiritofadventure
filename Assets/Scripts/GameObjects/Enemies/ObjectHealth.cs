﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHealth : Health
{
    [SerializeField]
    protected GameObject[] Bonuses;

    protected EnemyStateMachine myStates;
    protected GameObject replacingObject;
    protected Rigidbody rig;
    protected LifeBar lifebar;
    protected int startHP;
    protected AudioClip deathAudioClip;
    protected AudioClip hitAudioClip;
    protected GameObject hitVFX;
    protected GameObject deathVFX;
    protected Animator m_Animator;

    public Action OnDeathAction;


    private void Start()
    {
        GetData();
    }

    protected virtual void Die()
    {
        rig.useGravity = false;
        lifebar.gameObject.SetActive(false);

        if (replacingObject)
        {
            Instantiate(replacingObject, transform.position, transform.rotation);
            CreateBonuses();
        }

        if (deathAudioClip)
        {
            AudioSource.PlayClipAtPoint(deathAudioClip, Camera.main.transform.position);
        }

        if (deathVFX)
        {
            Instantiate(deathVFX, transform.position, Quaternion.identity);
        }

        if (OnDeathAction != null)
        {
            OnDeathAction();
            if (myStates.isKilled && StarsController.Instance)
                StarsController.Instance.UpdateScore(startHP);
        }
        gameObject.layer = 9;
        GetComponent<Collider>().isTrigger = true;
        Destroy(gameObject, Time.deltaTime * 2);
    }

    protected void CreateBonuses()
    {
        for (int i = 0; i < Bonuses.Length; i++)
        {
            if (!Bonuses[i])
                return;

            Vector3 pos = transform.position;
            pos.z = transform.position.z + i * 2f;

            Instantiate(Bonuses[i], pos, Quaternion.identity);
        }
    }

    public override void SetHP(int value)
    {
        int i = m_hp > value ? -1 : 1;
        m_hp = value;
        if (m_hp <= 0)
        {
            Die();
        }
        lifebar.UpdateLifeBar(m_hp, startHP, i * (startHP - m_hp));
    }

    protected override void GetData()
    {
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        data = TutorialLevel.SceneObjectsData(data);
        m_hp = startHP = data.GetObjectByType(m_ObjectType).Object_health;
        Vector3 lifeBarPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.5f);
        lifebar = LifeBar.NewLifeBar(lifeBarPos, -0.6f);
        lifebar.transform.SetParent(transform, true);
        lifebar.SetLifeBar(m_hp, startHP);
        myStates = GetComponent<EnemyStateMachine>();
        rig = GetComponent<Rigidbody>();
        m_Animator = GetComponent<Animator>();
        replacingObject = data.GetObjectByType(m_ObjectType).Object_ReplacingObjectPrefab;
        deathAudioClip = data.GetObjectByType(m_ObjectType).Object_deathAudioClip;
        hitAudioClip = data.GetObjectByType(m_ObjectType).Object_hitAudioClip;
        hitVFX = data.GetObjectByType(m_ObjectType).Object_hitVFX;
        deathVFX = data.GetObjectByType(m_ObjectType).Object_deathVFX;
    }

    public override void TakeDamage(GameObject damageDealer, int value)
    {
        int currentHP = m_hp - value;
        m_hp = Mathf.Clamp(currentHP, 0, startHP);
        lifebar.UpdateLifeBar(m_hp, startHP, -value);

        if (m_hp <= 0)
        {
            Die();
            return;
        }

        if (m_Animator && value > 0)
        {
            if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle Tree"))
            {
                m_Animator.SetTrigger("hit");
            }
            if (hitVFX)
            {
                Instantiate(hitVFX, transform.position, Quaternion.identity);
            }
            if (hitAudioClip)
            {
                AudioSource.PlayClipAtPoint(hitAudioClip, Camera.main.transform.position);
            }
        }
    }

    public override void TakeHeal(GameObject Healer, int value)
    {
        if (startHP - m_hp < value)
            value = startHP - m_hp;
        int currentHP = m_hp + value;
        m_hp = Mathf.Clamp(currentHP, 0, startHP);
        lifebar.UpdateLifeBar(m_hp, startHP, value);
    }
}
