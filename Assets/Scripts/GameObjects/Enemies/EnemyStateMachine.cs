﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyMovement))]
public class EnemyStateMachine : MonoBehaviour, IEnemy
{
    public bool isKilled = true;

    public enum EEnemyState
    {
        Stay,
        Walk,
        Attack,
        Die
    }

    [SerializeField]
    private GameObject m_wallHitVFX;
    [SerializeField]
    public bool m_HaveAppearAnimation;

    public Action OnFinishMoveAction;

    protected EnemyMovement movement;
    protected int m_Damage = 0;
    protected EEnemyState state;
    protected float targetPosition;
    protected Coroutine attackCoroutine;
    protected Animator myAnimator;
    protected ObjectHealth myHealth;

    protected bool isReady = false;

    public bool IsReady { get { return isReady; } set { isReady = value; } }

    protected void Start()
    {
        myAnimator = GetComponent<Animator>();
        movement = GetComponent<EnemyMovement>();
        myHealth = GetComponent<ObjectHealth>();

        GameStateMachine.Instance.OnEnemyTurnAction += MoveForward;
        GameStateMachine.Instance.OnPlayerTurnAction += UnReady;
        myHealth.OnDeathAction += UnRegisterEnemy;

        state = EEnemyState.Stay;
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        data = TutorialLevel.SceneObjectsData(data);
        m_Damage = data.GetObjectByType(myHealth.M_ObjectType).Object_damage;
        RegisterEnemy();
        SetIdleAnimation();
    }

    protected void OnDisable()
    {
        GameStateMachine.Instance.OnEnemyTurnAction -= MoveForward;
        GameStateMachine.Instance.OnPlayerTurnAction -= UnReady;
        myHealth.OnDeathAction -= UnRegisterEnemy;
    }

    protected void ChangeState(EEnemyState newState)
    {
        if (newState != state)
        {
            state = newState;
            AnimationState(newState);
        }
    }

    protected void Update()
    {
        switch (state)
        {
            case EEnemyState.Stay:
                StayState();
                break;
            case EEnemyState.Walk:
                WalkState();
                break;
            case EEnemyState.Attack:
                AttackState();
                break;
            case EEnemyState.Die:
                DieState();
                break;
            default:
                break;
        }
    }

    public void RegisterEnemy()
    {
        SceneObjectsManager.Instance.Registrate(this);
    }

    protected void UnRegisterEnemy()
    {
        Invoke("UnRegisterate", Time.deltaTime);
    }

    private void UnRegisterate()
    {
        SceneObjectsManager.Instance.UnRegistrate(this);
    }

    public bool ReadyCheck()
    {
        return IsReady;
    }

    protected void UnReady()
    {
        IsReady = false;
    }

    protected void StayState()
    {
        //TODO - если не запущена анимация айдл - запустить        
    }

    protected virtual void WalkState()
    {
        if (transform.position.z <= 2.9f)
        {
            ChangeState(EEnemyState.Attack);
            return;
        }

        if (transform.position.z > targetPosition)
        {
            movement.MoveForward();
        }
        else
        {
            Vector3 pos = transform.position;
            pos.z = Mathf.Round(transform.position.z);
            transform.position = pos;

            ChangeState(EEnemyState.Stay);
            if (OnFinishMoveAction != null)
                OnFinishMoveAction();
            IsReady = true;
            SceneObjectsManager.Instance.UpdateEnemiesStatus();
        }
    }

    protected void AttackState()
    {
        if (attackCoroutine == null)
        {
            attackCoroutine = StartCoroutine(AttackCoroutine());
        }
    }

    protected void DieState()
    {
        if (myHealth.HP > 0)
            myHealth.SetHP(0);
        //Destroy(this);
    }

    public virtual void MoveForward()
    {
        if (m_HaveAppearAnimation)
        {
            return;
        }
        StartCoroutine(DelayCoroutine());
    }

    protected IEnumerator AttackCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        HitTheWall();
    }

    protected void HitTheWall()
    {
        isKilled = false;
        SceneManagerScript.Instance.player.GetComponent<Health>().TakeDamage(gameObject, m_Damage);
        Instantiate(m_wallHitVFX, new Vector3(transform.position.x, transform.position.y, transform.position.z - 1f), Quaternion.identity);
        ChangeState(EEnemyState.Die);
    }

    protected void AnimationState(EEnemyState state)
    {
        if (!myAnimator)
            return;
        switch (state)
        {
            case EEnemyState.Stay:
                myAnimator.SetBool("move", false);
                break;
            case EEnemyState.Walk:
                myAnimator.SetBool("move", true);
                break;
            case EEnemyState.Attack:
                myAnimator.SetBool("move", false);
                break;
            case EEnemyState.Die:
                break;
            default:
                break;
        }
    }

    public void SetIdleAnimation()
    {
        int nm = UnityEngine.Random.Range(0, 5);
        myAnimator.SetFloat("IdleState", nm);
    }

    private IEnumerator DelayCoroutine()
    {
        yield return new WaitForSeconds(0.8f);
        targetPosition = Mathf.Round(transform.position.z - 1f);
        ChangeState(EEnemyState.Walk);
    }
}
