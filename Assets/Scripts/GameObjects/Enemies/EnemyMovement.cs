﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    private float speed = 1;

    public void MoveForward()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        return;
    }
}

