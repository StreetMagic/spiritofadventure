﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyKidHealth : ObjectHealth
{
    private Collider myCollider;

    private void Start()
    {
        GetData();
    }

    protected override void GetData()
    {
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        m_hp = startHP = data.GetObjectByType(m_ObjectType).Object_health;
        myStates = GetComponent<EnemyStateMachine>();
        rig = GetComponent<Rigidbody>();
        myCollider = GetComponent<Collider>();
        m_Animator = GetComponent<Animator>();
        deathAudioClip = data.GetObjectByType(m_ObjectType).Object_deathAudioClip;
        hitAudioClip = data.GetObjectByType(m_ObjectType).Object_hitAudioClip;
        hitVFX = data.GetObjectByType(m_ObjectType).Object_hitVFX;
        deathVFX = data.GetObjectByType(m_ObjectType).Object_deathVFX;
    }

    public void ActivateJelly(int health)
    {
        m_hp = startHP = health;
        Vector3 lifeBarPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.5f);
        lifebar = LifeBar.NewLifeBar(lifeBarPos, -0.6f);
        lifebar.transform.SetParent(transform, true);
        lifebar.SetLifeBar(m_hp, startHP);
        myCollider.enabled = true;
    }
}
