﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDestroyer : MonoBehaviour
{
    private List<BallBehavior> balls = new List<BallBehavior>();

    private void Start()
    {
        GameStateMachine.Instance.OnPlayerTurnAction += ClearBalls;
    }

    private void OnDestroy()
    {
        GameStateMachine.Instance.OnPlayerTurnAction -= ClearBalls;
    }

    private void Update()
    {
        for (int i = balls.Count - 1; i >= 0; i--)
        {
            if (balls[i].isActiveAndEnabled == false)
            {
                balls.Remove(balls[i]);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        BallBehavior ball = other.gameObject.GetComponent<BallBehavior>();
        if (ball)
        {
            if (balls.Contains(ball))
            {
                DestroyBall(ball.gameObject);
                balls.Remove(ball);
                return;
            }
            else
            {
                balls.Add(ball);
            }
        }
    }

    private void DestroyBall(GameObject ball)
    {
        ball.SetActive(false);
        BallsManager.Instance.RemoveBall(ball);
    }

    private void ClearBalls()
    {
        balls.Clear();
    }
}
