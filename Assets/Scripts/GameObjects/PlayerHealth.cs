﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : Health
{
    private int startHP;

    // Use this for initialization
    void Start()
    {
        SceneManagerScript.Instance.OnLevelBuilt += GetData;
    }

    private void OnDestroy()
    {
        SceneManagerScript.Instance.OnLevelBuilt -= GetData;
    }

    public override void SetHP(int value)
    {
        m_hp = value;
        HealthUI.Instance.SetPlayerHP(m_hp, startHP, startHP- m_hp);
    }

    protected override void GetData()
    {
        if (SceneManager.GetActiveScene().name == "Tutorial_SampleScene")
        {
            startHP = m_hp = 5;
        }
        else startHP = m_hp = Resources.Load<PlayerCharacterData>("ObjectsDatabases/PlayerData").PlayerLifes;
        SetHP(m_hp);
    }

    public override void TakeDamage(GameObject damageDealer, int value)
    {
        m_hp -= value;
        HealthUI.Instance.SetPlayerHP(m_hp, startHP, -value);
        if (m_hp <= 0)
        {
            SceneManagerScript.Instance.OnLoseAction();
        }
    }

    public override void TakeHeal(GameObject Healer, int value)
    {
        m_hp += value;
        HealthUI.Instance.SetPlayerHP(m_hp, startHP, value);
    }
}
