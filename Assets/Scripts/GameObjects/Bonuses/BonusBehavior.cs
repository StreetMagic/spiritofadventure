﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BonusBehavior : MonoBehaviour, IBonus
{
    protected bool isReady = false;

    abstract public bool ReadyCheck();
    abstract public void RegisterBonus();
}
