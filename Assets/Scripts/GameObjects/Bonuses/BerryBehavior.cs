﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BerryBehavior : BonusBehavior
{
    [SerializeField]
    private float speed = 1;
    [SerializeField]
    private GameObject m_bonusBerry;

    public Action OnFinishMoveAction;

    private EnemyMovement myMovement;
    private float targetPosition;
    private Animator myAnimator;

    private int extraBallDuration;
    private BallInfo extraBallInfo;
    private GameObject DeathVFX;
    private Camera mainCam;

    // Use this for initialization
    void Start()
    {
        mainCam = SceneManagerScript.Instance.m_mainCamera;
        RegisterBonus();
        myAnimator = GetComponent<Animator>();
        SceneManagerScript.Instance.OnLevelBuilt += GetData;
        GetData();
        //GameStateMachine.Instance.OnEnemyTurnAction += StartMovement;
        //GameStateMachine.Instance.OnPlayerTurnAction += UnReady;
    }


    private void OnDestroy()
    {
        SceneManagerScript.Instance.OnLevelBuilt -= GetData;
        //GameStateMachine.Instance.OnEnemyTurnAction -= StartMovement;
        //GameStateMachine.Instance.OnPlayerTurnAction -= UnReady;
    }

    private void GetData()
    {
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        BallType enm = (BallType)Enum.ToObject(typeof(BallType), data.GetObjectByType(ObjectType.berry).Object_extraBallType);
        extraBallInfo = Resources.Load<BallsData>("ObjectsDatabases/BallsDatabase").GetBallByType(enm);
        extraBallDuration = data.GetObjectByType(ObjectType.berry).Object_extraBallDuration;
    }

    private void UnReady()
    {
        isReady = false;
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if(other.GetComponent<BallBehavior>())
    //    {
    //        BallsManager.Instance.AddBallToStack(extraBallInfo, extraBallDuration);
    //        SceneObjectsManager.Instance.UnRegistrate(this);
    //        Destroy(gameObject);
    //    }
    //}

    private void StartMovement()
    {
        StartCoroutine(MoveCoroutine());
    }

    private IEnumerator MoveCoroutine()
    {
        targetPosition = Mathf.Round(transform.position.z - 1f);
        yield return new WaitForSeconds(0.8f);


        while (true)
        {
            if (transform.position.z <= 2.9f)
            {
                SceneObjectsManager.Instance.UnRegistrate(this);
                Destroy(gameObject);
                break;
            }

            if (transform.position.z > targetPosition)
            {
                transform.Translate(Vector3.forward * Time.deltaTime * speed);
                yield return null;
            }
            else
            {
                Vector3 pos = transform.position;
                pos.z = Mathf.Round(transform.position.z);
                transform.position = pos;

                if (OnFinishMoveAction != null)
                    OnFinishMoveAction();
                isReady = true;
                SceneObjectsManager.Instance.UpdateBonusesStatus();
                break;
            }
        }
    }

    public override bool ReadyCheck()
    {
        return isReady;
    }

    public override void RegisterBonus()
    {
        SceneObjectsManager.Instance.Registrate(this);
    }

    public void ActivateBerry()
    {
        Vector3 position = mainCam.WorldToScreenPoint(transform.position+ Vector3.up*2);
        Instantiate(m_bonusBerry, position, Quaternion.identity, SceneManagerScript.Instance.m_gameCanvas.transform);
        BallsManager.Instance.AddBallToStack(extraBallInfo, extraBallDuration);
        SceneObjectsManager.Instance.UnRegistrate(this);

        Destroy(gameObject);
    }

    //public void ActivateBerry()
    //{
    //    gameObject.layer = 0;
    //    isReady = true;
    //}
}
