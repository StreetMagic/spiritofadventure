﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusMoney : MonoBehaviour
{
    private EnemyStateMachine myStateMachine;
    private ObjectHealth myHealth;

    private void Start()
    {
        myStateMachine = GetComponent<EnemyStateMachine>();
        myHealth = GetComponent<ObjectHealth>();
        myHealth.OnDeathAction += SpawnCoin;
    }

    private void OnDestroy()
    {
        myHealth.OnDeathAction -= SpawnCoin;
    }

    private void SpawnCoin()
    {
        if (myStateMachine.isKilled)
        {
            CoinsController.Instance.SpawnCoins(transform.position);
        }
    }
}
