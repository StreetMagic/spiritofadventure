﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartObjectAtLevel : MonoBehaviour
{
    private Vector3 targetPosition;
    private Vector3 startPosition;
    float lerp;

    private Rigidbody rig;

    public void SetObjectPosition(Vector3 position)
    {
        targetPosition = position;
        transform.position = position + new Vector3(0f, -2f, 0f);
        transform.localScale = Vector3.zero;
        startPosition = transform.position;

        rig = GetComponent<Rigidbody>();
        rig.isKinematic = true;
    }
	// Update is called once per frame
	void Update ()
    {
        lerp += Time.deltaTime * 3f;
        transform.position = Vector3.Lerp(startPosition, targetPosition, lerp);
        transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, lerp);
		if(lerp >= 1f)
        {
            transform.position = targetPosition;
            transform.localScale = Vector3.one;
            Destroy(this);
            rig.isKinematic = false;
        }
	}
}
