﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBehavior : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve AnimationCurve;

    private int coins = 0;
    private Action OnCollectAction;

    private void Start()
    {
        OnCollectAction += CollectCoin;
        GetComponent<Animator>().SetTrigger("Collect");
        Tweener.TweenToPosition(transform, transform.position, SceneManagerScript.Instance.m_coinCollectPoint.position, 2f, OnCollectAction, AnimationCurve);
    }

    private void OnDestroy()
    {
        OnCollectAction -= CollectCoin;
    }
    private void CollectCoin()
    {
        SceneManagerScript.Instance.UpdateMoney(coins);
        Destroy(gameObject);
    }

    public void SetMoney(int money)
    {
        coins = money;
    }
}
