﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeAnimation : MonoBehaviour
{
    private Vector3 startAngles;

    void Start()
    {
        startAngles = transform.eulerAngles;
        StartCoroutine(AnimateProcess());        
    }

    private IEnumerator AnimateProcess()
    {        
        Quaternion rotation = transform.rotation;

        transform.eulerAngles = startAngles;

        Vector3 rotateAngles;
        rotateAngles.x = Random.Range(-5f, 5f);
        rotateAngles.y = Random.Range(-5f, 5f);
        rotateAngles.z = Random.Range(-5f, 5f);

        transform.Rotate(rotateAngles);

        Quaternion targetRotation = transform.rotation;
        transform.rotation = rotation;

        float timer = 0f;
        float targetTimer = Random.Range(2f, 5f);

        while (timer < targetTimer)
        {
            transform.rotation = Quaternion.Lerp(rotation, targetRotation, timer / targetTimer);

            timer += Time.deltaTime;
            yield return null;
        }

        StartCoroutine(AnimateProcess());
    }
}
