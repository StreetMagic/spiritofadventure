﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectType { chest, groot, stoneCube, bushCube, bomb, knight, ram, jelly, gunner, berry,  powerBall, ghostBall, tornado};
public abstract class Health : MonoBehaviour
{
    [SerializeField]
    protected ObjectType m_ObjectType;

    protected int m_hp = 1;

    public int HP { get { return m_hp; } }

    public ObjectType M_ObjectType { get { return m_ObjectType; } }

    public abstract void TakeDamage(GameObject damageDealer, int value);

    public abstract void TakeHeal(GameObject Healer, int value);

    public abstract void SetHP(int value);

    protected abstract void GetData();
}
