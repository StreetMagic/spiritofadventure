﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallBehavior : MonoBehaviour
{
    public float Speed;
    private float baseSpeed;

    public int Damage = 1;
    private int baseDamage;

    [SerializeField]
    private AudioSource m_audioBallRolling;

    private GameObject prevWall;
    private GameObject prevObject;

    private Vector3 currentVelocity;

    private Rigidbody rig;

    private Dictionary<GameObject, float> DamagedObjects = new Dictionary<GameObject, float>();

    private void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    void OnEnable()
    {
        BallsManager.Instance.AddBall(gameObject);
    }

    private void Update()
    {
        Vector3 rotationAxis = Vector3.Cross(rig.velocity.normalized, Vector3.up);
        transform.rotation *= Quaternion.AngleAxis(BallsManager.Instance.m_ballsRotationSpeed *Time.deltaTime, -rotationAxis);
    }

    private void FixedUpdate()
    {
        Vector3 vel = rig.velocity;
        if (transform.position.y > 0.25f)
        {
            vel.y = -20f;
        }
        else
        {
            vel.y = 0f;
        }
        rig.velocity = vel;

        if (rig.isKinematic)
            return;
        //выравнивание скорости шара
        Vector3 vec = rig.velocity;
        vec = vec.normalized * Speed;
        rig.velocity = Vector3.Lerp(rig.velocity, vec, Time.deltaTime * 10f);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "WallCollider")
        {
            if (prevWall)
            {
                prevWall = null;
            }
            else
            {
                prevWall = collision.gameObject;
                Vector3 velocity = rig.velocity;
                Vector3 normal = collision.transform.forward;
                rig.isKinematic = true;
                velocity = Vector3.Reflect(velocity, normal);
                rig.isKinematic = false;
                rig.velocity = velocity;
            }
        }
        //else if (!collision.gameObject.GetComponent<GroundObject>())
        //{
        //    if (prevObject)
        //    {
        //        prevObject = null;
        //    }
        //    else
        //    {
        //        prevObject = collision.gameObject;
        //        print(prevObject);
        //        Vector3 velocity = rig.velocity;
        //        Vector3 normal = collision.contacts[0].normal;
        //        print(normal);
        //        rig.isKinematic = true;
        //        velocity = Vector3.Reflect(velocity, normal);
        //        rig.isKinematic = false;
        //        rig.velocity = velocity;
        //    }
        //}

        ProcessAudioAndVFX(collision.gameObject);

        SetDamage(collision.gameObject);
    }

    private void ProcessAudioAndVFX(GameObject m_collisionObject)
    {
        if (m_collisionObject.GetComponent<GroundObject>())
        {
            m_audioBallRolling.Play();
        }
    }

    void SetDamage(GameObject obj)
    {
        var health = obj.GetComponent<Health>();
        if (!health)
            return;
        if (TargetCheck(obj))
            health.TakeDamage(gameObject, Damage);
        else
        {
            print("too much damage");
        }
    }

    private bool TargetCheck(GameObject obj)
    {
        float time = 0;
        DamagedObjects.TryGetValue(obj, out time);
        if (time == 0)
        {
            DamagedObjects.Add(obj, Time.time);
        }
        return Time.time - time > 0.05f;
    }
}
