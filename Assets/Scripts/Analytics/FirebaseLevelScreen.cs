﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseLevelScreen : MonoBehaviour
{
    void Start()
    {
        if (FirebaseScreenController.Instance)
            FirebaseScreenController.Instance.ActivateScreen(ScreenTypes.level);
    }
}
