﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ScreenTypes {none, mainMenu, shop, levelSelection, level, pause, settings }
public class FirebaseScreenController : MonoBehaviour
{
    public static FirebaseScreenController Instance { get; private set; }
    private ScreenTypes lastScreen;
    private ScreenTypes activeScreen;
    private ScreenTypes ActiveScreen
    {
        get { return activeScreen; }
        set
        {
            if (activeScreen != value)
            {
                activeScreen = value;
                SendAnalytics(activeScreen);
            }
        }
    }

    private void SendAnalytics(ScreenTypes screen)
    {
        Firebase.Analytics.FirebaseAnalytics.SetCurrentScreen(screen.ToString(), screen.ToString());
        print(screen.ToString());
    }

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
        DontDestroyOnLoad(this);
    }

    public void ActivateScreen(ScreenTypes screen)
    {
        lastScreen = ActiveScreen;
        ActiveScreen = screen;
    }

    public void DiactivateScreen()
    {
        ActiveScreen = lastScreen;
    }
}
