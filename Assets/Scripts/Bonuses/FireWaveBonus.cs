﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FireWaveBonus : BasicBonus
{
    [Range(1, 10)]
    [SerializeField]
    private int m_waveSpeed;
    [Range(1, 40)]
    [SerializeField]
    private int m_waveDamage;
    [SerializeField]
    private float m_leftBorderX = -2f;
    [SerializeField]
    private float m_RightBorderX = 3f;
    [SerializeField]
    private Vector3 m_StartPosition = new Vector3(0, 1f, 3f);
    [SerializeField]
    private float m_EndPositionX = 11.5f;
    [SerializeField]
    private Text m_ButtonText;

    [SerializeField]
    private Sprite m_InactiveIcon;

    private AudioSource audioSource;
    private BonusData data;
    private Image iconImage;
    private IEnumerator myCoroutine;
    private float cooldownTimer;
    private float positionX;
    private int chargesCount = 5;
    private bool isActivated = false;

    private void Awake()
    {
        data = Resources.Load<BonusesDatabase>("ObjectsDatabases/BonusesDatabase").GetObjectByType(type);
        iconImage = GetComponent<Image>();

        int number = int.Parse(PlayerPrefs.GetString("CurrentLevel").Remove(0, 7));
        if (number < 11)
        {
            iconImage.sprite = m_InactiveIcon;
        }
        else
        {
            iconImage.sprite = data.Bonus_Icon;
        }
        cooldownTimer = data.Bonus_Cooldown;
        audioSource = GetComponent<AudioSource>();

    }
    // Use this for initialization
    void Start()
    {
        int value = ProgressDataController.Instance.GetData("FireWall");
        if (value >= 0)
            chargesCount = value;
        m_ButtonText.text = chargesCount.ToString();
        InputManager.Instance.TapDownAction += StartBonusEffect;
        GameStateMachine.Instance.OnPlayerTurnAction+= UpdateCooldown;
    }

    private void OnDestroy()
    {
        InputManager.Instance.TapDownAction -= StartBonusEffect;
        GameStateMachine.Instance.OnPlayerTurnAction -= UpdateCooldown;
    }

    private void Update()
    {
        if (cooldownTimer > 0 && myCoroutine == null)
        {
            cooldownTimer -= Time.deltaTime;
        }
    }

    public override void ActivateBonus()
    {
        if (BallsManager.Instance.ActiveBalls.Count > 0)
            return;

        if (SceneManagerScript.Instance.player.IsPushingBall)
            return;

        if (chargesCount <= 0 || myCoroutine != null || cooldownTimer > 0)
            return;

        int number = int.Parse(PlayerPrefs.GetString("CurrentLevel").Remove(0, 7));
        if (number < 11)
        {
            return;
        }

        if (isActivated)
        {
            DeactivateBonus();
            return;
        }

        isActivated = true;
        BonusManager.Instance.ActiveBonus = this;
        iconImage.sprite = data.Bonus_ActivationIcon;
        InputManager.Instance.IsTargetingMode = true;
    }

    public override void AddBonus()
    {
        chargesCount++;
        m_ButtonText.text = chargesCount.ToString();
        ProgressDataController.Instance.SetData("FireWall", chargesCount);
    }

    public override void SpendBonus()
    {
        chargesCount--;
        m_ButtonText.text = chargesCount.ToString();
        ProgressDataController.Instance.SetData("FireWall", chargesCount);
    }

    private void StartBonusEffect(InputData inputData)
    {
        if (!isActivated)
            return;

        RaycastHit hit;
        Ray ray = SceneManagerScript.Instance.m_mainCamera.ScreenPointToRay(inputData.StartPositon);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.point.z < -0.5f)
            {
                return;
            }

            float currentX = Mathf.Clamp(Mathf.Round(hit.point.x), m_leftBorderX, m_RightBorderX);
            positionX = currentX;
        }

        if (myCoroutine == null)
        {
            myCoroutine = FireWaveCoroutine();
            StartCoroutine(myCoroutine);
        }
    }

    private IEnumerator FireWaveCoroutine()
    {
        SpendBonus();

        Vector3 pos = m_StartPosition;
        pos.x = positionX;
        GameObject fireWave = Instantiate(data.Bonus_Prefab, pos, Quaternion.identity);
        fireWave.GetComponent<FireWaveObject>().SetParameters(m_waveSpeed, m_waveDamage);
        audioSource.Play();
        while (fireWave.transform.position.z < m_EndPositionX)
        {
            yield return null;
        }
        Destroy(fireWave);
        audioSource.Stop();
        cooldownTimer = data.Bonus_Cooldown;
        myCoroutine = null;
        DeactivateBonus();
    }

    public override void DeactivateBonus()
    {
        if (myCoroutine != null)
            return;
        isActivated = false;
        iconImage.sprite = data.Bonus_Icon;
        InputManager.Instance.IsTargetingMode = false;
    }

    private void UpdateCooldown()
    {
        if (cooldownTimer > 0)
            cooldownTimer--;
    }
}
