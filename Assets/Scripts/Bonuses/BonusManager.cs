﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusManager : MonoBehaviour
{
    public static BonusManager Instance { get; private set; }

    private BasicBonus currentActiveBonus;
    public BasicBonus ActiveBonus
    {
        get { return currentActiveBonus; }
        set
        {
            if (currentActiveBonus != value)
            {
                if (currentActiveBonus)
                    currentActiveBonus.DeactivateBonus();
                currentActiveBonus = value;
            }
        }
    }


    // Use this for initialization
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}
