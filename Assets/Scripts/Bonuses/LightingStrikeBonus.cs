﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightingStrikeBonus : BasicBonus
{
    [Range(1, 40)]
    [SerializeField]
    private int m_Damage;
    [SerializeField]
    private float m_leftBorderX = -2f;
    [SerializeField]
    private float m_RightBorderX = 3f;
    [SerializeField]
    private float m_topBorderZ = 11f;
    [SerializeField]
    private float m_botBorderZ = 3f;
    [SerializeField]
    private Text m_ButtonText;

    [SerializeField]
    private Sprite m_InactiveIcon;

    private BonusData data;
    private Image iconImage;
    private IEnumerator myCoroutine;
    private int cooldownTimer;
    private int chargesCount;
    private bool isActivated = false;

    private void Awake()
    {
        data = Resources.Load<BonusesDatabase>("ObjectsDatabases/BonusesDatabase").GetObjectByType(type);
        iconImage = GetComponent<Image>();
        int number = int.Parse(PlayerPrefs.GetString("CurrentLevel").Remove(0, 7));
        if (number < 6)
        {
            iconImage.sprite = m_InactiveIcon;
        }
        else
        {
            iconImage.sprite = data.Bonus_Icon;
        }

        cooldownTimer = data.Bonus_Cooldown;
    }

    void Start()
    {
        int value = ProgressDataController.Instance.GetData("FireBall");
        if (value >= 0)
            chargesCount = value;
        m_ButtonText.text = chargesCount.ToString();
        InputManager.Instance.TapDownAction += StartBonusEffect;
        GameStateMachine.Instance.OnPlayerTurnAction += UpdateCooldown;
    }

    private void OnDestroy()
    {
        InputManager.Instance.TapDownAction -= StartBonusEffect;
        GameStateMachine.Instance.OnPlayerTurnAction -= UpdateCooldown;
    }

    public override void ActivateBonus()
    {
        if (BallsManager.Instance.ActiveBalls.Count > 0)
            return;

        if (SceneManagerScript.Instance.player.IsPushingBall)
            return;

        if (SceneObjectsManager.Instance.Enemies.Count <= 0)
            return;

        if (chargesCount <= 0 || myCoroutine != null || cooldownTimer > 0)
            return;

        int number = int.Parse(PlayerPrefs.GetString("CurrentLevel").Remove(0, 7));
        if (number < 6)
        {
            return;
        }

        if (isActivated)
        {
            DeactivateBonus();
            return;
        }

        isActivated = true;
        BonusManager.Instance.ActiveBonus = this;
        iconImage.sprite = data.Bonus_ActivationIcon;
        InputManager.Instance.IsTargetingMode = true;
    }

    public override void AddBonus()
    {
        chargesCount++;
        m_ButtonText.text = chargesCount.ToString();
        ProgressDataController.Instance.SetData("FireBall", chargesCount);
    }

    public override void SpendBonus()
    {
        chargesCount--;
        m_ButtonText.text = chargesCount.ToString();
        ProgressDataController.Instance.SetData("FireBall", chargesCount);
    }

    private void StartBonusEffect(InputData inputData)
    {
        Vector3 targetPos;
        if (!isActivated)
            return;

        RaycastHit hit;
        Ray ray = SceneManagerScript.Instance.m_mainCamera.ScreenPointToRay(inputData.StartPositon);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.point.z < -0.5f)
            {
                return;
            }

            float currentX = Mathf.Clamp(Mathf.Round(hit.point.x), m_leftBorderX, m_RightBorderX);
            float currentZ = Mathf.Clamp(Mathf.Round(hit.point.z + hit.point.y * Mathf.Cos(1.0472f)), m_botBorderZ, m_topBorderZ);
            targetPos = new Vector3(currentX, 0.5f, currentZ);
        }
        else return;

        Collider[] targets = Physics.OverlapSphere(targetPos, 0.4f);
        List<GameObject> enemies = new List<GameObject>();
        foreach (var target in targets)
        {
            if (target.GetComponent<EnemyStateMachine>())
            {
                enemies.Add(target.gameObject);
            }
        }

        if (enemies.Count <= 0)
            return;

        if (myCoroutine == null)
        {
            myCoroutine = LightningStrikeCoroutine(enemies, targetPos);
            StartCoroutine(myCoroutine);
        }
    }

    private IEnumerator LightningStrikeCoroutine(List<GameObject> enemies, Vector3 position)
    {
        yield return new WaitForSeconds(0.3f);

        AudioSource.PlayClipAtPoint(data.Bonus_Audio, SceneManagerScript.Instance.m_mainCamera.transform.position);
        Instantiate(data.Bonus_Prefab, position, data.Bonus_Prefab.transform.rotation);

        for (int i = 0; i < enemies.Count; i++)
        {
            var health = enemies[i].GetComponent<Health>();
            if (health)
            {
                health.TakeDamage(gameObject, m_Damage);
            }
        }
        SpendBonus();
        cooldownTimer = data.Bonus_Cooldown;
        myCoroutine = null;
        DeactivateBonus();
    }

    public override void DeactivateBonus()
    {
        if (myCoroutine != null)
        {
            return;
        }
        isActivated = false;
        iconImage.sprite = data.Bonus_Icon;
        InputManager.Instance.IsTargetingMode = false;
    }

    private void UpdateCooldown()
    {
        if (cooldownTimer > 0)
            cooldownTimer--;
    }
}
