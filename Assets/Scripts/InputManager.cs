﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public struct InputData
{
    public StateInput State;
    public SwipeDirections swipeDirection;
    public Vector2 StartPositon;
    public Vector2 CurrentPosition;
    public Vector2 EndPosition;
    public float Duration;
}

public enum StateInput { tap, tapDown, swipe, longTap, none }
public enum SwipeDirections
{
    none, up, down, right, left,
}

public class InputManager : MonoBehaviour
{
    public static InputManager Instance { get; private set; }

    [SerializeField]
    private float m_swipeMinDistance = 40f;
    [SerializeField]
    private float m_longPressTime = 0.1f;

    public SwipeDirections SwipeDirection { get; private set; }
    public Vector2 CurrentPosition { get; private set; }

    private static Vector2 touchStartPosition;
    private static Vector2 touchEndPosition;
    private static float startTouchTime;
    private static float endTouchTime;
    private static float pressTimer;
    private static float minSqrMagnitude;
    private static InputData currentData;

    public Action<InputData> TapDownAction;
    public Action<InputData> TapAction;
    public Action<InputData> LongPressAction;
    public Action<InputData> SwipeAction;

    //[HideInInspector]
    public bool IsTutorialMode = false;
    //[HideInInspector]
    public bool IsTargetingMode = false;

    // Use this for initialization
    void Awake()
    {
        minSqrMagnitude = m_swipeMinDistance * m_swipeMinDistance;
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (IsTutorialMode)
            return;
#if UNITY_EDITOR
        EditorUpdate();
#endif

#if UNITY_IOS
        MobileUpdate();
#endif

#if UNITY_ANDROID
        MobileUpdate();
#endif

    }

    private bool SwipeDetection()
    {
        Vector2 direction = touchEndPosition - touchStartPosition;
        if (direction.sqrMagnitude > minSqrMagnitude)
        {
            if (Mathf.Abs(direction.x) >= Mathf.Abs(direction.y))
            {
                SwipeDirection = direction.x > 0 ? SwipeDirections.right : SwipeDirections.left;
            }
            else
            {
                SwipeDirection = direction.y > 0 ? SwipeDirections.up : SwipeDirections.down;
            }
            return true;
        }
        return false;
    }


    private InputData SetInputData()
    {
        InputData newSwipeData = new InputData();
        newSwipeData.State = StateInput.swipe;
        newSwipeData.swipeDirection = SwipeDirection;
        newSwipeData.StartPositon = touchStartPosition;
        newSwipeData.EndPosition = touchEndPosition;
        newSwipeData.Duration = endTouchTime - startTouchTime;

        return newSwipeData;
    }

    private void ResetTouch()
    {
        //touchStartPosition = Vector2.zero;
        //touchEndPosition = Vector2.zero;
        startTouchTime = 0f;
        endTouchTime = 0f;
        pressTimer = 0f;
    }

    private void EditorUpdate()
    {
        CurrentPosition = Input.mousePosition;

        if (Input.GetMouseButtonDown(0))
        {
            startTouchTime = Time.time;
            touchStartPosition = Input.mousePosition;
            if (TapDownAction != null)
            {
                currentData = new InputData();
                currentData.State = StateInput.tapDown;
                currentData.swipeDirection = SwipeDirections.none;
                currentData.StartPositon = Input.mousePosition;
                TapDownAction(currentData);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            endTouchTime = Time.time;
            touchEndPosition = Input.mousePosition;
            if (SwipeDetection() && SwipeAction != null)
            {
                currentData = SetInputData();
                SwipeAction(currentData);
            }
            else if (TapAction != null)
            {
                currentData = new InputData();
                currentData.State = StateInput.tap;
                currentData.swipeDirection = SwipeDirections.none;
                currentData.StartPositon = touchStartPosition;
                currentData.EndPosition = touchEndPosition;
                TapAction(currentData);
            }
            ResetTouch();
        }

        if (Input.GetMouseButton(0))
        {
            pressTimer += Time.deltaTime;
            touchEndPosition = Input.mousePosition;
            if (pressTimer > m_longPressTime)
            {
                if (LongPressAction != null)
                {
                    currentData = new InputData();
                    currentData.State = StateInput.longTap;
                    currentData.swipeDirection = SwipeDirections.none;
                    currentData.StartPositon = touchStartPosition;
                    currentData.CurrentPosition = CurrentPosition;
                    currentData.EndPosition = touchEndPosition;
                    currentData.Duration = Time.time - startTouchTime;
                    LongPressAction(currentData);
                }

                if (currentData.State != StateInput.longTap)
                    pressTimer = 0;
            }
        }
    }

    private void MobileUpdate()
    {
        if (Input.touches.Length == 0)
            return;

        if (EventSystem.current.IsPointerOverGameObject())
            return;

        CurrentPosition = Input.touches[0].position;

        if (Input.touches[0].phase == TouchPhase.Began)
        {
            startTouchTime = Time.time;
            touchStartPosition = Input.touches[0].position;
            if (TapDownAction != null)
            {
                currentData = new InputData();
                currentData.State = StateInput.tapDown;
                currentData.swipeDirection = SwipeDirections.none;
                currentData.StartPositon = touchStartPosition;
                TapDownAction(currentData);
            }
        }

        if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
        {
            endTouchTime = Time.time;
            touchEndPosition = Input.touches[0].position;
            if (SwipeDetection() && SwipeAction != null)
            {
                SwipeAction(SetInputData());
            }
            else if (TapAction != null)
            {
                currentData = new InputData();
                currentData.State = StateInput.tap;
                currentData.swipeDirection = SwipeDirections.none;
                currentData.StartPositon = touchStartPosition;
                currentData.EndPosition = touchEndPosition;
                TapAction(currentData);
            }
            ResetTouch();
        }

        if (Input.touches[0].phase == TouchPhase.Stationary || Input.touches[0].phase == TouchPhase.Moved)
        {
            pressTimer += Time.deltaTime;
            touchEndPosition = Input.touches[0].position;
            if (pressTimer > m_longPressTime)
            {
                if (LongPressAction != null)
                {
                    currentData = new InputData();
                    currentData.State = StateInput.longTap;
                    currentData.swipeDirection = SwipeDirections.none;
                    currentData.StartPositon = touchStartPosition;
                    currentData.CurrentPosition = CurrentPosition;
                    currentData.EndPosition = touchEndPosition;
                    currentData.Duration = Time.time - startTouchTime;
                    LongPressAction(currentData);
                }

                if (currentData.State != StateInput.longTap)
                    pressTimer = 0;
            }
        }
    }
}



