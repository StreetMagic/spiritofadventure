﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, IPlayer
{
    [SerializeField]
    private Transform m_ballStartPoint;

    [SerializeField]
    private int m_zonesCount;

    [SerializeField]
    private float m_ballPushImpulse = 10f;

    [SerializeField]
    private Vector3 m_rightBorder = new Vector3(3.5f, 0f, 3.1f);

    [SerializeField]
    private Vector3 m_leftBorder = new Vector3(-2.5f, 0f, 3.1f);

    [SerializeField]
    private GameObject m_ballEffect;

    private Camera mainCamera;
    private IEnumerator movementCoroutine;
    private IEnumerator ballCoroutine;
    private Animator myAnimator;
    private TrajectoryCalculator myTrajectory;

    private float zoneSize;
    private float[] zoneCenters;
    private float currentZoneCenter;

    private bool isAiming = false;
    private bool isShooting = false;
    public BallInfo BallInfo;

    public bool IsPushingBall { get; private set; }
    public int BallsAtOnce { get; set; }
    public float BetweenBallsTimer { get; set; }

    private GameObject throwBallEffect;
    private StackBallData[] currentBallsStack;

    private void Start()
    {
        RegisterPlayer();
        mainCamera = SceneManagerScript.Instance.m_mainCamera;
        myAnimator = GetComponent<Animator>();
        myTrajectory = GetComponent<TrajectoryCalculator>();
        SceneManagerScript.Instance.OnZoneChangeAction += GetZones;
        SceneManagerScript.Instance.OnLevelBuilt += GetData;
        InputManager.Instance.TapAction += PushBalls;
        InputManager.Instance.LongPressAction += ObjectRotation;
        SceneManagerScript.Instance.OnVictoryAction += Victory;
        SceneManagerScript.Instance.OnLoseAction += Lose;
        GameStateMachine.Instance.OnPlayerTurnAction += ReadyToShoot;
    }

    private void OnDestroy()
    {
        SceneManagerScript.Instance.OnLevelBuilt -= GetData;
        InputManager.Instance.LongPressAction -= ObjectRotation;
        InputManager.Instance.TapAction -= PushBalls;
        SceneManagerScript.Instance.OnZoneChangeAction -= GetZones;
        SceneManagerScript.Instance.OnVictoryAction -= Victory;
        SceneManagerScript.Instance.OnLoseAction -= Lose;
        GameStateMachine.Instance.OnPlayerTurnAction -= ReadyToShoot;
    }

    private void ReadyToShoot()
    {
        isShooting = false;
    }

    private void GetData()
    {
        BallInfo = Resources.Load<BallsData>("ObjectsDatabases/BallsDatabase").GetBallByType(BallType.basic);
        BetweenBallsTimer = Resources.Load<PlayerCharacterData>("ObjectsDatabases/PlayerData").BetweenBallsTimer;
    }


    private void GetZones(BoxCollider zone)
    {
        float wholeDistance = zone.size.x;
        zoneSize = wholeDistance / m_zonesCount;
        float leftEdgeX = zone.center.x - zone.size.x * 0.5f + 0.5f;
        zoneCenters = new float[m_zonesCount];
        for (int i = 0; i < zoneCenters.Length; i++)
        {
            zoneCenters[i] = leftEdgeX + 0.5f * zoneSize + zoneSize * i;
        }
    }

    private void ObjectRotation(InputData data)
    {
        if (GameStateMachine.Instance.CurrentState != GameStates.PlayerTurn)
            return;

        if (BallsManager.Instance.ActiveBalls.Count > 0 || InputManager.Instance.IsTargetingMode)
            return;

        if (movementCoroutine != null || isShooting)
            return;

        if (SceneObjectsManager.Instance.Enemies.Count <= 0)
            return;

        Vector2 positionPlayerScreen = mainCamera.WorldToScreenPoint(transform.position);

        if (data.CurrentPosition.y < positionPlayerScreen.y)
        {
            ObjectReturnDefaultRotation();
            myAnimator.speed = 1;
            myTrajectory.TurnOffLine();
            return;
        }

        isAiming = true;
        myAnimator.speed = 0;
        Vector2 directionUp = positionPlayerScreen - (new Vector2(positionPlayerScreen.x, Screen.height));
        Vector2 directionMouse = positionPlayerScreen - data.CurrentPosition;
        float angle = Vector2.Angle(directionMouse, directionUp);
        if (data.CurrentPosition.x < positionPlayerScreen.x) angle = 0 - angle;
        Vector3 pos = transform.position;
        pos.y = 0f;
        float leftLimit = Vector3.Angle(Vector3.forward, m_leftBorder - pos);
        float rightLimit = Vector3.Angle(Vector3.forward, m_rightBorder - pos);
        angle = Mathf.Clamp(angle, -leftLimit, rightLimit);
        transform.rotation = Quaternion.Euler(0, angle, 0);

        myTrajectory.Raycast();
    }

    private void ObjectReturnDefaultRotation()
    {
        transform.rotation = Quaternion.Euler(0, 0, 0);
        isAiming = false;
    }

    private IEnumerator MovementCoroutine(float center)
    {
        float deltaXLeft = Mathf.Abs(center - transform.position.x);
        while (deltaXLeft > 0.1f)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.position = new Vector3(transform.position.x, 0, 0.6f);
            if (center > transform.position.x)
            {
                myAnimator.SetFloat("Speed", 1);
            }
            else if (center < transform.position.x)
            {
                myAnimator.SetFloat("Speed", -1);
            }
            deltaXLeft = Mathf.Abs(center - transform.position.x);
            yield return null;
        }
        myAnimator.SetFloat("Speed", 0);
        movementCoroutine = null;
    }

    private void PushBalls(InputData data)
    {
        if (!isAiming)
            return;

        if (isShooting)
            return;

        if (ballCoroutine != null)
            return;

        IsPushingBall = true;
        isShooting = true;
        isAiming = false;
        myAnimator.speed = 1;
        myAnimator.SetTrigger("Hit");
    }

    // ЗАПУСК ИЗ АНИМАЦИИ  по триггеру Hit
    public void StartShootingBalls()
    {
        if (ballCoroutine != null)
            return;

        currentBallsStack = BallsManager.Instance.BallsStack.ToArray();
        for (int i = 0; i < BallsManager.Instance.BallsStack.Count; i++)
        {
            currentBallsStack[i] = BallsManager.Instance.BallsStack[i];
        }
        ballCoroutine = PushBallsCoroutine();
        StartCoroutine(ballCoroutine);
        throwBallEffect = Instantiate(m_ballEffect, m_ballStartPoint.position, m_ballStartPoint.rotation);
    }

    IEnumerator PushBallsCoroutine()
    {
        foreach (var ball in currentBallsStack)
        {
            yield return new WaitForSeconds(BetweenBallsTimer);
            PushBall(ball.info);
        }

        IsPushingBall = false;
        Destroy(throwBallEffect, 0.4f);
        yield return new WaitForSeconds(0.1f);
        myAnimator.SetTrigger("Hit");
        yield return new WaitForSeconds(0.4f);
        ObjectReturnDefaultRotation();
        BallsManager.Instance.UpdateRoundNumber();
        ballCoroutine = null;
    }

    public void PushBall(BallInfo ballType)
    {
        GameObject ballObject = BuildBallObject(ballType);

        Rigidbody rig = ballObject.GetComponent<Rigidbody>();

        rig.isKinematic = false;
        rig.velocity = Vector3.zero;
        ballObject.transform.parent = null;
        rig.AddForce(transform.forward * m_ballPushImpulse, ForceMode.Impulse);
        myTrajectory.TurnOffLine();

    }

    private GameObject BuildBallObject(BallInfo ballInfo)
    {
        GameObject ballObj = ObjectsPool.Instance.GetObject(ballInfo.Ball_Prefab);
        ballObj.transform.position = m_ballStartPoint.position;
        //ballObj.transform.rotation = m_ballStartPoint.rotation;

        BallBehavior ball = ballObj.GetComponent<BallBehavior>();
        if (SceneManager.GetActiveScene().name == "Tutorial_SampleScene")
        {
            ball.Speed = 11f;
            ball.Damage = 1;
            return ballObj;
        }
        ball.Speed = ballInfo.Ball_speed;
        ball.Damage = ballInfo.Ball_damage;

        return ballObj;
    }

    public void RegisterPlayer()
    {
        SceneManagerScript.Instance.player = this;
    }

    public void MoveToPosition(float x)
    {
        ObjectReturnDefaultRotation();
        if (TutorialLevel.isPlaying) return;
        float compareX = zoneSize;
        for (int i = 0; i < zoneCenters.Length; i++)
        {
            float currentDeltaX = Mathf.Abs(x - zoneCenters[i]);
            if (currentDeltaX < compareX)
            {
                compareX = currentDeltaX;
                currentZoneCenter = zoneCenters[i];
            }
        }
        if (movementCoroutine != null)
        {
            StopCoroutine(movementCoroutine);
        }
        movementCoroutine = MovementCoroutine(currentZoneCenter);
        StartCoroutine(movementCoroutine);
    }

    private void Victory()
    {
        // количество уровней в игре
        TextAsset[] assets = Resources.LoadAll<TextAsset>("Levels/");
        if (assets != null)
        {
            // текущий уровень
            string currLevel = PlayerPrefs.GetString("CurrentLevel");
            int level = 0;
            Int32.TryParse(currLevel.Split('/')[1], out level);
            string levelsDecode = PlayerPrefs.GetString("AcessLevels");
            string levelStarsDecode = PlayerPrefs.GetString("LevelsStars");
            //дешифровка строки из настроек
            string levels = B64X.DecodeToString(levelsDecode);
            string levelStars = B64X.DecodeToString(levelStarsDecode);
            // массив данных о пройденых уровнях
            List<string> userData = new List<string>();
            List<string> userStarsData = new List<string>();
            // дешифрование не вернуло нустую строку
            if (!String.IsNullOrEmpty(levels))
                userData = levels.Split(',').ToList();
            if (!String.IsNullOrEmpty(levelStars))
                userStarsData = levelStars.Split(',').ToList();
            if (userData.Count < assets.Length)
            {
                var length = assets.Length;
                //заполняем недостающие уровни
                for (int i = userData.Count; i < length; i++) // i = 3 ; 3 < 10 ; i++
                {
                    userData.Add("0");
                }
            }

            if (userStarsData.Count < assets.Length)
            {
                var length = assets.Length;
                //заполняем недостающие уровни
                for (int i = userStarsData.Count; i < length; i++) // i = 3 ; 3 < 10 ; i++
                {
                    userStarsData.Add("0");
                }
            }
            StarsController.Instance.LastTotalStars = int.Parse(userStarsData[level - 1]);
            int stars = StarsController.Instance.TotalStars;
            //в тукущий уровень проставляем победу
            if (level < userData.Count)
            {
                userData[level] = "1";
                if (stars > StarsController.Instance.LastTotalStars)
                    userStarsData[level - 1] = stars.ToString();
            }
            string userDataJoin = string.Join(",", userData.ToArray());
            string userStarsDataJoin = string.Join(",", userStarsData.ToArray());
            string levelsEncode = B64X.EncodeFrom(userDataJoin);
            string starsEncode = B64X.EncodeFrom(userStarsDataJoin);
            PlayerPrefs.SetString("AcessLevels", levelsEncode);
            PlayerPrefs.SetString("LevelsStars", starsEncode);
        }
        else
        {
            Debug.LogError("Levels not found!");
        }
        StartCoroutine(VictoryCoroutine());
    }

    private void Lose()
    {
        StopAllCoroutines();
        StartCoroutine(LoseCoroutine());
    }

    private IEnumerator VictoryCoroutine()
    {
        while (ballCoroutine != null)
        {
            yield return null;
        }

        myAnimator.SetTrigger("Win");
        while (BallsManager.Instance.ActiveBalls.Count > 0)
        {
            yield return null;
        }
        DlgWndController.instace.Victory();
    }

    private IEnumerator LoseCoroutine()
    {
        myAnimator.SetTrigger("Lose");
        while (transform.position.z > -2.5f)
        {
            yield return null;
        }
        if (SceneManager.GetActiveScene().name == "Tutorial_SampleScene")
        {
            DlgWndController.instace.TutorialFailed();
            CustomAnalytics.Instance.FailedTutorial();
        }
        else
            DlgWndController.instace.Defeat();
    }
}
