﻿using UnityEngine;
using System.Collections;
using System;

//Здесь описываем кодинг стандарт
public class CodingStandart : MonoBehaviour
{//фигурные скобки начинаем с новой строки
    [SerializeField] private float m_floatVariable;     //поля для редактора начинаем с m_
    public float FloatVariable //проперти пишем с большой буквы
    {
        get { return m_floatVariable; }
        set { m_floatVariable = value; }
    }

    private float myPrivateFloat;               //локальные переменные начинаем с маленькой буквы

    private Action someAction;

    public Action<float> FloatAction;


    private void Reset()
    {
        m_floatVariable = 1f;                   //здесь указываем значения по умолчанию для переменных - вызывается при добавлении на объект
    }                                           //можно вызвать из контексного меню редактора что бы сбросить поля
    private void OnEnable()
    {
        someAction += DoSomthing;
        FloatAction += FloatActionMethod;
    }


    // Use this for initialization
    private void Start()
    {
        someAction.Invoke();
        FloatAction.Invoke(5f);
    }

    // Update is called once per frame
    private void Update()
    {
        //если апдейт и старт не используются - их необходимо удалить
    }

    private void OnDisable()
    {
        someAction -= DoSomthing;
        FloatAction -= FloatActionMethod;
    }

    //этот метод ничего не делает
    public void DoSomthing()
    {

    }

    //этот локальный метод ничего не делает
    private void DoSomthing(int i)
    {

    }

    private void FloatActionMethod(float value)
    {

    }
}
