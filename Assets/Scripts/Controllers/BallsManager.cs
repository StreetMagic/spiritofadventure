﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BallType { basic, berry }

public class BallsManager : MonoBehaviour
{
    public static BallsManager Instance { get; private set; }

    //TODO remove from here
    [Range(500, 10000)]
    [SerializeField]
    public int m_ballsRotationSpeed;

    private BallsData ballsDatabase;
    private BallInfo ballInfo;
    private PlayerController player;
    private int roundNumber = 1;

    public List<GameObject> ActiveBalls = new List<GameObject>();
    public List<StackBallData> BallsStack = new List<StackBallData>();

    public Action OnChangeStackAction;

    private int baseBallsCount;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        ActiveBalls = new List<GameObject>();
        SceneManagerScript.Instance.OnLevelBuilt += GetData;
        //GameStateMachine.Instance.OnEnemyTurnAction += UpdateRoundNumber;
        TutorialLevel.OnLevelBuilt();
    }

    public void UpdateRoundNumber()
    {
        roundNumber++;
        for (int i = BallsStack.Count - 1; i >= 0; i--)
        {
            if (BallsStack[i].duration == roundNumber)
            {
                BallsStack.Remove(BallsStack[i]);
                if (OnChangeStackAction != null)
                    OnChangeStackAction();
            }
        }
    }

    private void OnDestroy()
    {
        SceneManagerScript.Instance.OnLevelBuilt -= GetData;
        //GameStateMachine.Instance.OnEnemyTurnAction -= UpdateRoundNumber;
    }

    public void AddBall(GameObject ball)
    {
        if (!ActiveBalls.Contains(ball))
        {
            ActiveBalls.Add(ball);
        }
    }

    public void RemoveBall(GameObject ball)
    {
        if (ActiveBalls.Contains(ball))
        {
            ActiveBalls.Remove(ball);
        }
        if (ActiveBalls.Count <= 0 && !player.IsPushingBall)
        {
            GameStateMachine.Instance.ChangeState(GameStates.EnemyTurn);
            player.MoveToPosition(ball.transform.position.x);
        }
    }

    public void DestroyBalls()
    {
        if (player.IsPushingBall)
            return;

        if (ActiveBalls.Count <= 0)
            return;

        GameObject lastBall = ActiveBalls[ActiveBalls.Count - 1];
        if (!lastBall)
            return;

        foreach (var ball in ActiveBalls)
        {
            ball.SetActive(false);
        }

        ActiveBalls = new List<GameObject>();
        player.MoveToPosition(lastBall.transform.position.x);
        GameStateMachine.Instance.ChangeState(GameStates.EnemyTurn);
    }

    public void AddBallToStack(BallInfo ball, int duration)
    {
        StackBallData data = new StackBallData();
        data.info = ball;
        data.duration = roundNumber + duration;
        BallsStack.Add(data);
        if (OnChangeStackAction != null)
            OnChangeStackAction();
    }

    public void RemoveBallFromStack(BallInfo info)
    {
        foreach (var ball in BallsStack)
        {
            if (ball.info == info)
            {
                BallsStack.Remove(ball);
                if (OnChangeStackAction != null)
                    OnChangeStackAction();
            }
        }
    }

    public void ResetBallStack()
    {
        BallsStack.Clear();
        for (int i = 0; i < baseBallsCount; i++)
        {
            StackBallData data = new StackBallData();
            data.info = ballInfo;
            data.duration = 100;
            BallsStack.Add(data);
        }
    }

    private void GetData()
    {
        ballsDatabase = Resources.Load<BallsData>("ObjectsDatabases/BallsDatabase");
        ballInfo = ballsDatabase.GetBallByType(BallType.basic);
        player = SceneManagerScript.Instance.player;
        baseBallsCount = Resources.Load<PlayerCharacterData>("ObjectsDatabases/PlayerData").BallsPerTurn;

        //tutor
        baseBallsCount = TutorialLevel.BallsCount(baseBallsCount);

        for (int i = 0; i < baseBallsCount; i++)
        {
            StackBallData data = new StackBallData();
            data.info = ballInfo;
            data.duration = 100;
            BallsStack.Add(data);
        }
    }
}

[Serializable]
public class StackBallData
{
    public BallInfo info;
    public int duration;
}