﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections;

public class MinecraftMovementController : MonoBehaviour
{
    public enum MinecraftAction { none, build, destroy }

    [SerializeField]
    private Transform m_startCameraPos;
    [SerializeField]
    private Transform m_prefabPreviewPos;
    [SerializeField]
    private float m_rotationSpeed = 2f;
    [SerializeField]
    private float m_movementSpeed = 0.3f;

    private GameObject objectPreview;
    private GameObject root;
    private int nm;
    private int oldNm;

    private GameObject[] prefabs;
    private Camera mainCamera;

    // Use this for initialization
    void Start()
    {
        mainCamera = Camera.main;
        string[] assets = AssetDatabase.FindAssets("t:GameObject", new[] { "Assets/Resources/LevelPrefabs/LevelObjects" });
        prefabs = new GameObject[assets.Length];
        for (int i = 0; i < prefabs.Length; i++)
        {
            string path = AssetDatabase.GUIDToAssetPath(assets[i]);
            path = path.Split('.')[0];
            path = path.Remove(0, 17);
            prefabs[i] = Resources.Load<GameObject>(path);
        }

        objectPreview = Instantiate(prefabs[0], m_prefabPreviewPos.position, m_prefabPreviewPos.rotation) as GameObject;
        objectPreview.transform.eulerAngles = new Vector3(10, 130, -30);
        objectPreview.transform.parent = m_prefabPreviewPos;
        Rigidbody rig = objectPreview.GetComponent<Rigidbody>();
        if (rig)
            rig.isKinematic = true;
        root = GameObject.FindGameObjectWithTag("LevelRoot");
        if (!root)
        {
            root = new GameObject();
            root.tag = "LevelRoot";
            root.name = "LevelRoot";
        }
    }

    // Update is called once per frame
    void Update()
    {
        //camera rotation
        transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Mouse X") * m_rotationSpeed);
        transform.RotateAround(transform.position, transform.right * -1, Input.GetAxis("Mouse Y") * m_rotationSpeed);

        //camera movement
        float h = Input.GetAxis("Horizontal") * m_movementSpeed;
        float v = Input.GetAxis("Vertical") * m_movementSpeed;
        float j = 0;
        if (Input.GetKey(KeyCode.E)) j = m_movementSpeed;
        if (Input.GetKey(KeyCode.Q)) j = -m_movementSpeed;


        transform.Translate(transform.forward * v, Space.World);
        transform.Translate(Vector3.up * j, Space.World);
        transform.Translate(transform.right * h, Space.World);

        //change blocks
        float scrl = Input.mouseScrollDelta.y;
        if (scrl != 0)
        {
            if (scrl > 0) nm += 1; else nm -= 1;
            nm = Mathf.Clamp(nm, 0, prefabs.Length - 1);
        }

        //build or desrtoy blocks
        MinecraftAction action = MinecraftAction.none;
        GameObject TargetBlock = null;
        if (Input.GetMouseButtonUp(0)) action = MinecraftAction.build; //build new block
        if (Input.GetMouseButtonUp(1)) action = MinecraftAction.destroy; //destroy block

        if (action != MinecraftAction.none)
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                Transform hitTransform = hit.transform;
                //while (hitTransform.parent != hitTransform.root)
                //{
                //    hitTransform = hitTransform.parent;
                //}
                TargetBlock = hitTransform.gameObject;

                if (action == MinecraftAction.build)
                {
                    FindBlockPosition(hit.point, TargetBlock);
                }

                if (action == MinecraftAction.destroy && TargetBlock.GetComponent<LevelEditorObject>().isDestroyable)
                {

                    Destroy(TargetBlock);
                }
            }
        }

        if (nm != oldNm)
        {
            Destroy(objectPreview);
            objectPreview = Instantiate(prefabs[nm], m_prefabPreviewPos.position, m_prefabPreviewPos.rotation) as GameObject;
            objectPreview.transform.eulerAngles = new Vector3(10, 130, -30);
            objectPreview.transform.parent = m_prefabPreviewPos;
            Rigidbody rig = objectPreview.GetComponent<Rigidbody>();
            if (rig)
                rig.isKinematic = true;
            oldNm = nm;
        }
    }

    void FindBlockPosition(Vector3 HitPosition, GameObject HitObject)
    {
        LevelEditorObject hitEditorObject = HitObject.GetComponent<LevelEditorObject>();
        Vector3 hitObjectCenter;
        if (HitObject.GetComponent<Collider>())
        {
            hitObjectCenter = HitObject.GetComponent<Collider>().bounds.center;
        }
        else
        {
            hitObjectCenter = new Vector3(HitObject.transform.position.x, HitObject.transform.position.y + hitEditorObject.size.y * 0.5f, HitObject.transform.position.z);
        }
        Vector3 finalPosition = new Vector3();
        GameObject obj;
        Collider[] objects;

        if (hitEditorObject && hitEditorObject.size == Vector3.zero)
        {
            finalPosition.y = 0f;
            finalPosition.x = Mathf.Round(HitPosition.x);
            finalPosition.z = Mathf.Round(HitPosition.z);
            objects = Physics.OverlapBox((finalPosition + Vector3.up * prefabs[nm].GetComponent<LevelEditorObject>().size.y * 0.5f),
                0.45f * prefabs[nm].GetComponent<LevelEditorObject>().size);

            if (objects.Length > 0)
            {
                return;
            }
            else
            {
                obj = Instantiate(prefabs[nm], finalPosition, prefabs[nm].transform.rotation) as GameObject;
                obj.transform.parent = root.transform;
                return;
            }
        }

        if (Mathf.Abs(hitObjectCenter.x - HitPosition.x) >= Mathf.Abs(hitObjectCenter.z - HitPosition.z))
        {
            if (Mathf.Abs(hitObjectCenter.x - HitPosition.x) >= Mathf.Abs(hitObjectCenter.y - HitPosition.y))
            {
                if (hitObjectCenter.x < HitPosition.x)
                {
                    finalPosition.x = HitPosition.x + prefabs[nm].GetComponent<LevelEditorObject>().size.x * 0.5f;
                    finalPosition.x = Mathf.Round(finalPosition.x);
                    finalPosition.y = HitObject.transform.position.y;
                    finalPosition.z = HitObject.transform.position.z;
                }
                else
                {
                    finalPosition.x = HitPosition.x - prefabs[nm].GetComponent<LevelEditorObject>().size.x * 0.5f;
                    finalPosition.x = Mathf.Round(finalPosition.x);
                    finalPosition.y = HitObject.transform.position.y;
                    finalPosition.z = HitObject.transform.position.z;
                }
            }
            else
            {
                if (hitObjectCenter.y < HitPosition.y)
                {
                    finalPosition.y = HitPosition.y;
                    finalPosition.x = HitObject.transform.position.x;
                    finalPosition.z = HitObject.transform.position.z;
                }
                else
                {
                    finalPosition.y = HitPosition.y - hitEditorObject.size.y;
                    finalPosition.x = HitObject.transform.position.x;
                    finalPosition.z = HitObject.transform.position.z;
                }
            }
        }
        else
        {
            if (Mathf.Abs(hitObjectCenter.z - HitPosition.z) >= Mathf.Abs(hitObjectCenter.y - HitPosition.y))
            {
                if (hitObjectCenter.z < HitPosition.z)
                {
                    finalPosition.z = HitPosition.z + prefabs[nm].GetComponent<LevelEditorObject>().size.z * 0.5f;
                    finalPosition.z = Mathf.Ceil(finalPosition.z);
                    finalPosition.y = HitObject.transform.position.y;
                    finalPosition.x = HitObject.transform.position.x;
                }
                else
                {
                    finalPosition.z = HitPosition.z - prefabs[nm].GetComponent<LevelEditorObject>().size.z * 0.5f;
                    finalPosition.z = Mathf.Floor(finalPosition.z);
                    finalPosition.y = HitObject.transform.position.y;
                    finalPosition.x = HitObject.transform.position.x;
                }
            }
            else
            {
                if (hitObjectCenter.y < HitPosition.y)
                {
                    finalPosition.y = HitPosition.y;
                    finalPosition.x = HitObject.transform.position.x;
                    finalPosition.z = HitObject.transform.position.z;
                }
                else
                {
                    finalPosition.y = HitPosition.y - hitEditorObject.size.y;
                    finalPosition.x = HitObject.transform.position.x;
                    finalPosition.z = HitObject.transform.position.z;
                }
            }
        }


        Vector3 boxCenter = new Vector3(finalPosition.x, finalPosition.y + prefabs[nm].GetComponent<LevelEditorObject>().size.y * 0.5f, finalPosition.z);
        objects = Physics.OverlapBox(boxCenter, 0.49f * prefabs[nm].GetComponent<LevelEditorObject>().size);

        if (objects.Length > 0)
        {
            print("something is here");
            return;
        }
        obj = Instantiate(prefabs[nm], finalPosition, prefabs[nm].transform.rotation) as GameObject;
        obj.transform.parent = root.transform;
    }

    public void SetCameraPos()
    {
        transform.position = m_startCameraPos.position;
        transform.rotation = m_startCameraPos.rotation;
    }
}
#endif