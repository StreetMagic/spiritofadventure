﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsController : MonoBehaviour
{
    public static CoinsController Instance { get; private set; }

    [SerializeField]
    private GameObject m_coin;

    private int coins;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        SceneManagerScript.Instance.OnLevelBuilt += GetData;
    }

    private void OnDestroy()
    {
        SceneManagerScript.Instance.OnLevelBuilt -= GetData;
    }

    private void GetData()
    {
        coins = Resources.Load<PlayerCharacterData>("ObjectsDatabases/PlayerData").CoinsPerStar;
    }


    public void SpawnCoins(Vector3 position)
    {
        StartCoroutine(SpawnCoroutine(position));
    }

    private IEnumerator SpawnCoroutine(Vector3 position)
    {
        int i = 5;
        while (i > 0)
        {
            GameObject coin = Instantiate(m_coin, position + new Vector3(Random.Range(-1, 1f), 0, Random.Range(-1f, 1f)), Random.rotation);
            coin.GetComponent<CoinBehavior>().SetMoney(coins/5);
            i--;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
