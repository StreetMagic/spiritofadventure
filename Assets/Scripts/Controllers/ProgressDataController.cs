﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ProgressDataController : CreateSingletonGameObject<ProgressDataController>
{
    private Dictionary<string, int> SavedProgress = new Dictionary<string, int>();

    private ProgressData progressData;
    private string path;

    void Awake()
    {

        DontDestroyOnLoad(this);

        path = Application.persistentDataPath + "/SavedProgress.json";

        if (File.Exists(path))
        {
            progressData = JsonUtility.FromJson<ProgressData>(File.ReadAllText(path));
        }
        else
        {
            TextAsset asset = Resources.Load<TextAsset>("SavedProgress");
            progressData = JsonUtility.FromJson<ProgressData>(asset.ToString());
        }
        SavedProgress.Clear();
        if (progressData != null)
        {
            for (int i = 0; i < progressData.savedData.Length; i++)
            {
                SavedProgress.Add(progressData.savedData[i].key, progressData.savedData[i].value);
            }
        }
    }


    public int GetData(string key)
    {
        int value = -1;
        if (SavedProgress.ContainsKey(key))
        {
            value = SavedProgress[key];
        }
        return value;
    }

    public void SetData(string key, int value)
    {
        if (SavedProgress.ContainsKey(key))
        {
            SavedProgress[key] = value;
        }
        else
        {
            SavedProgress.Add(key, value);
        }
        SaveData();
    }

    private void SaveData()
    {
        List<ProgressDataItem> newDataItems = new List<ProgressDataItem>();

        foreach (var save in SavedProgress)
        {
            ProgressDataItem savedData = new ProgressDataItem();
            savedData.key = save.Key;
            savedData.value = save.Value;
            newDataItems.Add(savedData);
        }

        progressData = new ProgressData();
        progressData.savedData = new ProgressDataItem[newDataItems.Count];
        for (int i = 0; i < newDataItems.Count; i++)
        {
            progressData.savedData[i] = newDataItems[i];
        }

        string dataAsText = JsonUtility.ToJson(progressData);
        File.WriteAllText(path, dataAsText);
        //File.WriteAllText("Assets/Resources/SavedProgress.json", dataAsText);
    }
}
