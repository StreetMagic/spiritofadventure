﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarsController : MonoBehaviour
{
    public static StarsController Instance { get; private set; }

    [SerializeField]
    private GameObject m_secondStar;

    [SerializeField]
    private GameObject m_thirdStar;

    public int TotalStars;
    public int LastTotalStars;

    private PlayerHealth player;
    private int monsterKilled = 0;
    private int monsterPoints = 0;
    private int lifesPoints = 0;

    private int monstersTotalCount;
    private int secondStarLimit;
    private int thirdStarLimit;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    void Start()
    {
        SceneManagerScript.Instance.OnLevelBuilt += GetData;
        GameStateMachine.Instance.OnPlayerTurnAction += ResetMonsterCount;
    }

    private void OnDestroy()
    {
        SceneManagerScript.Instance.OnLevelBuilt -= GetData;
        GameStateMachine.Instance.OnPlayerTurnAction -= ResetMonsterCount;
    }

    private void GetData()
    {
        player = SceneManagerScript.Instance.player.GetComponent<PlayerHealth>();
        CountStars();
    }

    private void ResetMonsterCount()
    {
        monsterKilled = 0;
    }

    public void UpdateScore(int HP)
    {
        monsterKilled++;
        monsterPoints += 10 * HP * monsterKilled;
        lifesPoints = 50 * ((1 + player.HP) * player.HP);
        CountStars();
    }

    public void UpdateScoreOnVictory()
    {
        monsterPoints += 50 * ((1 + player.HP) * player.HP);
        CountStars();
    }

    private void CountStars()
    {
        monstersTotalCount = LevelRoundLoader.Instance.TotalEnemiesCount;
        secondStarLimit = (monstersTotalCount - player.HP + 1) * 10 + 50 * ((1 + player.HP) * player.HP);
        thirdStarLimit = (monstersTotalCount - player.HP + 1) * 10 * 2 + 50 * ((1 + player.HP) * player.HP);

        int points = monsterPoints + lifesPoints;
        if (points <= secondStarLimit)
        {
            m_secondStar.SetActive(false);
            m_thirdStar.SetActive(false);
            TotalStars = 1;
        }
        else if (points < thirdStarLimit)
        {
            m_secondStar.SetActive(true);
            m_thirdStar.SetActive(false);
            TotalStars = 2;
        }
        else
        {
            m_secondStar.SetActive(true);
            m_thirdStar.SetActive(true);
            TotalStars = 3;
        }
    }
}
