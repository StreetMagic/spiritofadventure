﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController Instance;
    [SerializeField]
    private AudioSource m_clickSource;

    [SerializeField]
    private GameObject m_offSoundObject;
    [SerializeField]
    private GameObject m_offMusicObject;

    [SerializeField]
    private AudioSource m_musicSource;

    private AudioListener m_AudioListener;
    internal bool Sound
    {
        get
        {
            return AudioListener.volume != 0;
        }
    }
    internal bool Music
    {
        get
        {
            return m_musicSource.enabled;
        }
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(Instance.gameObject);
        }
        Instance = this;
    }
    void Start ()
    {
        bool soundVolume = PlayerPrefs.GetInt("Sound", 1) != 0;
        if (soundVolume)
            AudioListener.volume = 1;
        else
            AudioListener.volume = 0;

        m_musicSource.enabled = PlayerPrefs.GetInt("Music", 1) != 0;
    }
	

	void Update ()
    {
		if(Input.GetMouseButtonDown(0))
        {
            m_clickSource.Play(); 
        }
	}

    [ContextMenu("Sound On/Off")]
    public void OnOffSound()
    {
        bool soundOn = Sound;

        soundOn = !soundOn;

        if (soundOn)
        {
            AudioListener.volume = 1;
            m_musicSource.enabled = PlayerPrefs.GetInt("Music", 1) != 0;
        }
        else
        {
            AudioListener.volume = 0;
            m_musicSource.enabled = false;
            PlayerPrefs.SetInt("Music", 0);
        }
        PlayerPrefs.SetInt("Sound", (soundOn ? 1 : 0));
    }

    [ContextMenu("Music On/Off")]
    public void OnOffMusic()
    {
        bool musicOn = Music;

        musicOn = !musicOn;

        if (musicOn)
        {
            AudioListener.volume = 1;
            m_musicSource.enabled = true;
        }
        else
        {
            m_musicSource.enabled = false;
        }
        m_musicSource.enabled = musicOn;

        PlayerPrefs.SetInt("Music", musicOn ? 1 : 0);
    }
}
