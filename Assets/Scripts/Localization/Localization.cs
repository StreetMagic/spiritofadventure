﻿using UnityEngine;
using UnityEngine.UI;

public class Localization : MonoBehaviour
{
    private Text textComponent;
    private string key;

	void Awake ()
    {
        textComponent = GetComponent<Text>();
        if(!textComponent)
        {
            Destroy(this);
            return;
        }

        key = textComponent.text;
        LocalizationController.ChangeLocalization += ChangeLocalization;
	}

    private void OnDestroy()
    {
        LocalizationController.ChangeLocalization -= ChangeLocalization;
    }

    private void ChangeLocalization(string localization)
    {
        textComponent.text = LocalizationController.LocalizedText[key];
    }
}
