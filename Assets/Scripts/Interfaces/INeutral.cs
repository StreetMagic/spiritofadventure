﻿public interface INeutral
{
    void RegisterSubstance();
    bool ReadyCheck();
}
