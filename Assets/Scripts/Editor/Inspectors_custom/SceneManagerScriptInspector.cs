﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SceneManagerScript))]
public class SceneManagerScriptInspector : Editor
{
    public override void OnInspectorGUI()
    {
        SceneManagerScript myScript = (SceneManagerScript)target;
        var col = GUI.backgroundColor;
        GUI.backgroundColor = new Color32(255,255,102,255);
        if (GUILayout.Button("WIN LEVEL", GUILayout.Height(50)))
        {
            myScript.SetVictory();
        }
        GUILayout.Space(70);
        GUI.backgroundColor = Color.red;
        if (GUILayout.Button("Reset PlayerPrefs", GUILayout.Height(20), GUILayout.Width(150)))
        {
            PlayerPrefs.DeleteAll();
        }
        GUILayout.Space(50);
        GUI.backgroundColor = col;
        DrawDefaultInspector();
    }
}
