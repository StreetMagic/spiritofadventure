﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class LevelGenerationTool : EditorWindow
{
    private static int levelWidth = 8;
    private static int levelLength = 16;

    private static string[] SO_nameDatabase;
    private static string[] SO_pathDatabase;
    private static LevelGenerationData[] SO_types;
    private static LevelGenerationData levelData;
    private static Transform startGenerationPoint;

    public int index = 0;

    private void OnGUI()
    {
        GUILayout.Label("Level Types", EditorStyles.boldLabel);
        FindSO();
        levelWidth = (int)EditorGUILayout.Slider("Level Width", levelWidth, 4, 40);
        levelLength = (int)EditorGUILayout.Slider("Level Length", levelLength, 4, 40);
        index = EditorGUILayout.Popup(index, SO_nameDatabase);
        levelData = SO_types[index];
        if (GUILayout.Button("Generate level"))
        {
            GenerateLevel();
            Close();
        }
    }

    private static void FindSO()
    {
        string[] SO = AssetDatabase.FindAssets("t:LevelGenerationData");

        SO_pathDatabase = new string[SO.Length];
        SO_nameDatabase = new string[SO.Length];
        SO_types = new LevelGenerationData[SO.Length];
        for (int i = 0; i < SO.Length; i++)
        {
            string path = AssetDatabase.GUIDToAssetPath(SO[i]);
            path = path.Split('.')[0];
            path = path.Remove(0, 17);
            SO_pathDatabase[i] = path;
            SO_types[i] = Resources.Load<LevelGenerationData>(SO_pathDatabase[i]);
            SO_nameDatabase[i] = SO_types[i].name;
        }
    }

    private static void GenerateLevel()
    {
        GetRootObject();
        // gates
        //Instantiate(levelData.Gates, startGenerationPoint.position + startGenerationPoint.transform.up * 3, levelData.Gates.transform.rotation, startGenerationPoint);

        int elementsInRow = levelWidth / levelData.FloorElementSizeX;
        int elementsInColomn = levelLength / levelData.FloorElementSizeZ;

        int realLevelWidth = elementsInRow * levelData.FloorElementSizeX;
        int realLevelLength = elementsInColomn * levelData.FloorElementSizeZ;

        float posX = startGenerationPoint.position.x - ((elementsInRow - 1) * (levelData.FloorElementSizeX * 0.5f));
        float posZ = startGenerationPoint.position.z + (levelData.FloorElementSizeZ * 0.5f);

        for (int i = 0; i < elementsInColomn; i++)
        {
            for (int j = 0; j < elementsInRow; j++)
            {
                // floor
                int NM = Random.Range(0, levelData.FloorElement.Length);
                Vector3 pos = new Vector3(posX + levelData.FloorElementSizeX * j, startGenerationPoint.position.y - 0.5f, posZ + levelData.FloorElementSizeZ * i);
                Instantiate(levelData.FloorElement[NM], pos, levelData.FloorElement[NM].transform.rotation, startGenerationPoint);

            }
            // left border           
            Vector3 leftBorderPos = new Vector3(posX + levelData.FloorElementSizeX * elementsInRow, startGenerationPoint.position.y - 0.5f, posZ + levelData.FloorElementSizeZ * i);
            Instantiate(levelData.LeftBorderElement, leftBorderPos, levelData.LeftBorderElement.transform.rotation, startGenerationPoint);
            // right border
            Vector3 rightBorderPos = new Vector3(posX - levelData.FloorElementSizeX, startGenerationPoint.position.y - 0.5f, posZ + levelData.FloorElementSizeZ * i);
            Instantiate(levelData.RightBorderElement, rightBorderPos, levelData.RightBorderElement.transform.rotation, startGenerationPoint);
        }

        // front border
        for (int j = 0; j < elementsInRow; j++)
        {
            Vector3 frontBorderPos = new Vector3(posX + levelData.FloorElementSizeX * j, startGenerationPoint.position.y - 0.5f, posZ);
            Instantiate(levelData.FrontRightBorderElement, frontBorderPos, levelData.LeftBorderElement.transform.rotation, startGenerationPoint);
        }
        // Creating colliders

        //floor
        Vector3 floorColliderPos = new Vector3(startGenerationPoint.position.x, startGenerationPoint.position.y - 1f, startGenerationPoint.position.z + realLevelLength * 0.5f);
        GameObject floorCollider = Instantiate(levelData.FloorCollider, floorColliderPos, levelData.Collider.transform.rotation, startGenerationPoint);
        Vector3 floorColliderSize = new Vector3(realLevelWidth + levelData.FloorElementSizeX * 0.5f, 1f, realLevelLength + levelData.FloorElementSizeZ * 0.5f);
        floorCollider.GetComponent<BoxCollider>().size = floorColliderSize;
        //right side
        Vector3 rightColliderPos = new Vector3(startGenerationPoint.position.x + realLevelWidth * 0.5f + 0.5f, startGenerationPoint.position.y + 2f, startGenerationPoint.position.z + realLevelLength * 0.5f);
        GameObject rightCollider = Instantiate(levelData.Collider, rightColliderPos, levelData.Collider.transform.rotation, startGenerationPoint);
        Vector3 rightColliderSize = new Vector3(1f, 6f, realLevelLength + levelData.FloorElementSizeZ * 0.5f);
        rightCollider.GetComponent<BoxCollider>().size = rightColliderSize;
        rightCollider.tag = "WallCollider";
        // left side
        Vector3 leftColliderPos = new Vector3(startGenerationPoint.position.x - realLevelWidth * 0.5f - 0.5f, startGenerationPoint.position.y + 2f, startGenerationPoint.position.z + realLevelLength * 0.5f);
        GameObject leftCollider = Instantiate(levelData.Collider, leftColliderPos, levelData.Collider.transform.rotation, startGenerationPoint);
        Vector3 leftColliderSize = new Vector3(1f, 6f, realLevelLength + levelData.FloorElementSizeZ * 0.5f);
        leftCollider.GetComponent<BoxCollider>().size = leftColliderSize;
        leftCollider.tag = "WallCollider";
        // back side
        Vector3 backColliderPos = new Vector3(startGenerationPoint.position.x, startGenerationPoint.position.y + 2f, startGenerationPoint.position.z + realLevelLength);
        GameObject backCollider = Instantiate(levelData.Collider, backColliderPos, levelData.Collider.transform.rotation, startGenerationPoint);
        Vector3 backColliderSize = new Vector3(realLevelWidth + levelData.FloorElementSizeX * 0.5f, 6f, 1f);
        backCollider.GetComponent<BoxCollider>().size = backColliderSize;
        backCollider.tag = "WallCollider";
        // front side
        Vector3 frontColliderPos = new Vector3(startGenerationPoint.position.x, startGenerationPoint.position.y + 2f, startGenerationPoint.position.z);
        GameObject frontCollider = Instantiate(levelData.Collider, frontColliderPos, levelData.Collider.transform.rotation, startGenerationPoint);
        Vector3 frontColliderSize = new Vector3(realLevelWidth + levelData.FloorElementSizeX * 0.5f, 6f, 1f);
        frontCollider.GetComponent<BoxCollider>().size = frontColliderSize;
        frontCollider.tag = "WallCollider";

        //positioning complete level according to player position
        startGenerationPoint.Rotate(0f, 180f, 0f);
        startGenerationPoint.transform.Translate(0, 0.5f, -realLevelLength + 1f);

        // walkingzone
        GameObject walkingZone = Instantiate(levelData.Collider, new Vector3(0f, -2f, 0f), levelData.Collider.transform.rotation, startGenerationPoint);
        walkingZone.GetComponent<BoxCollider>().size = new Vector3(realLevelWidth, 0f, 0.2f);
        walkingZone.tag = "WalkingZone";
    }

    private static void GetRootObject()
    {
        if (GameObject.FindGameObjectWithTag("LevelRoot"))
        {
            DestroyImmediate(GameObject.FindGameObjectWithTag("LevelRoot"));
        }

        GameObject root = new GameObject();
        root.name = "startGenerationPoint";
        root.tag = "LevelRoot";
        root.transform.position = Vector3.zero;
        startGenerationPoint = root.transform;
    }

    public static void GenerateFixedSizeLevel()
    {
        //FindSO();
        //levelWidth = 6;
        //levelLength = 12;
        //levelData = SO_types[0];
        //GenerateLevel();
        GameObject levelRoot = GameObject.FindGameObjectWithTag("LevelRoot");
        GameObject levelBase;
        if (levelRoot)
        {
            if (levelRoot.transform.childCount == 0)
            {
                levelBase = Instantiate(Resources.Load<GameObject>("LevelPrefabs/LevelBase(for editor)"));
                levelBase.transform.parent = levelRoot.transform;
            }
        }
        else
        {
            levelBase = Instantiate(Resources.Load<GameObject>("LevelPrefabs/LevelBase(for editor)"));
            levelRoot = new GameObject();
            levelRoot.tag = "LevelRoot";
            levelRoot.name = "LevelRoot";
            levelRoot.transform.position = levelBase.transform.position;
            levelBase.transform.parent = levelRoot.transform;
        }
    }
}
