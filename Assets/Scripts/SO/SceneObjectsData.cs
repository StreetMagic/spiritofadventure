﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "SceneObjectsData", menuName = "Data/ObjectData", order = 1)]
public class SceneObjectsData : ScriptableObject
{
    public SceneObjectData[] ObjectsDatabase;

    public SceneObjectData GetObjectByID(int ID)
    {
        for (int i = 0; i < ObjectsDatabase.Length; i++)
        {
            if (ObjectsDatabase[i].Object_ID == ID)
            {
                return ObjectsDatabase[i];
            }
        }
        return null;
    }

    public SceneObjectData GetObjectByType(ObjectType type)
    {
        for (int i = 0; i < ObjectsDatabase.Length; i++)
        {
            if (ObjectsDatabase[i].Object_Type == type)
            {
                return ObjectsDatabase[i];
            }
        }
        return null;
    }

}

[Serializable]
public class SceneObjectData
{
    public int Object_ID;
    public ObjectType Object_Type;
    public GameObject Object_Prefab;
    public GameObject Object_ReplacingObjectPrefab;
    public AudioClip Object_deathAudioClip;
    public AudioClip Object_hitAudioClip;
    public GameObject Object_hitVFX;
    public GameObject Object_deathVFX;
    public string Object_name; 
    public int Object_moveDistance;
    public int Object_damage;
    public int Object_health;

    [ConditionalEnumHide("Object_Type", (int)ObjectType.groot, true)] public int Object_HealthRegen;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.gunner, true)] public int Object_ShotDamage;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.gunner, true)] public int Object_ShootDistance;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.bomb, true)] public int Object_ExplosionDamage;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.bomb, true)] public int Object_ExplosionRadius;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.jelly, true)] public int Object_FragmentsCount;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.berry, true)] public BallType Object_extraBallType;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.berry, true)] public int Object_extraBallDuration;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.knight, true)] public bool Object_FrontShield;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.knight, true)] public bool Object_BackShield;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.knight, true)] public bool Object_RightShield;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.knight, true)] public bool Object_LeftShield;


    public SceneObjectData(ObjectType type, GameObject prefab, GameObject replacingObject, AudioClip deathAudioClip, AudioClip hitAudioClip,
        GameObject hitVFX, GameObject deathVFX, string name, int distance, int damage, int health)
    {                                                                                      
        Object_Type = type;                                                                
        Object_Prefab = prefab;                                                            
        Object_name = name;
        Object_moveDistance = distance;
        Object_damage = damage;
        Object_health = health;
        Object_ReplacingObjectPrefab = replacingObject;
        Object_deathAudioClip = deathAudioClip;
        Object_hitAudioClip = hitAudioClip;
        Object_hitVFX = hitVFX;
        Object_deathVFX = deathVFX;
    }
}


