﻿using UnityEngine;

[CreateAssetMenu(fileName = "TutorialData", menuName = "Data/TutorialData", order = 1)]
[System.Serializable]
public class TutorialsData : ScriptableObject
{
    public TutorialLevelData[] Tutorials;
}

[System.Serializable]
public class TutorialLevelData
{
    public int LevelNumber;
    public Sprite[] TutorialSprites;
}
