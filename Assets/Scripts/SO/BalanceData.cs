﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class BalanceData
{
    public ObjectShortData[] ObjectsDatabase;
    public BallShortData[] BallsDatabase;

    public int BallsPerTurn;
    public float BetweenBallsTimer;
    public int PlayerLifes;
    public int CoinsInChest;
}

[System.Serializable]
public class ObjectShortData
{
    public ObjectType Object_Type;

    [Range(0, 10)]
    public int Damage;

    [Range(1, 400)]
    public int Health;

    [ConditionalEnumHide("Object_Type", (int)ObjectType.groot, true)] public int HealthRegenAmount;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.gunner, true)] public int ShotDamage;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.gunner, true)] public int ShootDistance;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.bomb, true)] public int ExplosionDamage;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.bomb, true)] public int ExplosionRadius;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.jelly, true)] public int FragmentsCount;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.berry, true)] public BallType ExtraBallType;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.berry, true)] public int ExtraBallDuration;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.knight, true)] public bool FrontShield;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.knight, true)] public bool BackShield;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.knight, true)] public bool RightShield;
    [ConditionalEnumHide("Object_Type", (int)ObjectType.knight, true)] public bool LeftShield;
}

[System.Serializable]
public class BallShortData
{
    public BallType Ball_Type;


    [Range(5, 30)]
    public float Speed;

    [Range(1, 30)]
    public int Damage;
}
