﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerCharacterData", menuName = "Data/PlayerData", order = 1)]
public class PlayerCharacterData : ScriptableObject
{
    public int BallsPerTurn;
    public int PlayerLifes;
    public float BetweenBallsTimer;
    public int CoinsPerStar;
}