﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using GameAnalyticsSDK;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private DlgAbstract m_DlgExit;
    [SerializeField]
    private DlgAbstract m_DlgSettings;
    [SerializeField]
    private DlgAbstract m_DlgLevel;
    [SerializeField]
    private DlgAbstract m_DlgShop;
    [SerializeField]
    private GameObject m_DlgMenu;

    [SerializeField]
    private GameObject m_MainPanel;

    [SerializeField]
    private GameObject m_AchivementButton;
    [SerializeField]
    private GameObject m_MapButton;
    [SerializeField]
    private GameObject m_ShopButton;

    private DlgAbstract Shop;
    private DlgAbstract Achivements;

    private void Awake()
    {
//        GameAnalytics.Initialize();
        DlgAbstract.DlgWasClosedEvent += DlgWasClosedEvent;
    }

    private void Start()
    {
//        FirebaseScreenController.Instance.ActivateScreen(ScreenTypes.mainMenu);
    }

    private void OnDestroy()
    {
        DlgAbstract.DlgWasClosedEvent -= DlgWasClosedEvent;
    }

    private void DlgWasClosedEvent(DlgAbstract obj)
    {
        //m_DlgMenu.SetActive(true);
    }

    public void LoadLevel()
    {
        SceneLoader.LoadLevel(new NextLevelData("SampleScene", true));
    }
    public void ShowDlgShop()
    {
        if (Shop)
            return;

        Shop = Instantiate(m_DlgShop, m_MainPanel.transform);
        Shop.OpenDlg();
        RescaleButtons(m_ShopButton);
    }

    public void ShowDlgMap()
    {
        if (Shop)
            Shop.CloseDlg();
        if (Achivements)
            Achivements.CloseDlg();

        RescaleButtons(m_MapButton);
    }

    public void ShowDlgSettings()
    {
        var dlg = Instantiate(m_DlgSettings, transform);
        //m_DlgMenu.SetActive(false);
        dlg.OpenDlg();
    }
    public void ShowDlgLevels()
    {
        var dlg = Instantiate(m_DlgLevel, transform);
        m_DlgMenu.SetActive(false);
        dlg.OpenDlg();
    }

    private void RescaleButtons(GameObject button)
    {
        m_AchivementButton.SetActive(false);
        m_MapButton.SetActive(false);
        m_ShopButton.SetActive(false);
        button.SetActive(true);
    }
}
