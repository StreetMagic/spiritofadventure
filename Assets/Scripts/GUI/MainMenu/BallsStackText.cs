﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallsStackText : MonoBehaviour
{
    private Text myText;

    private int basicBallsCount;
    private int berryBallsCount;

    private void Awake()
    {
        myText = GetComponent<Text>();
    }

    // Use this for initialization
    void Start()
    {
        Invoke("UpdateBallsStackText", 0.2f);
        BallsManager.Instance.OnChangeStackAction += UpdateBallsStackText;
    }

    private void OnDestroy()
    {
        BallsManager.Instance.OnChangeStackAction -= UpdateBallsStackText;
    }

    private void UpdateBallsStackText()
    {
        basicBallsCount = 0;
        berryBallsCount = 0;
        foreach (var item in BallsManager.Instance.BallsStack)
        {
            switch (item.info.Ball_Type)
            {
                case BallType.basic:
                    basicBallsCount++;
                    break;
                case BallType.berry:
                    berryBallsCount++;
                    break;
                default:
                    break;
            }
        }

        myText.text = basicBallsCount.ToString();
        if (berryBallsCount > 0)
            myText.text += "+" + "<color=#d3314b>" + berryBallsCount.ToString() + "</color>";
    }
}
