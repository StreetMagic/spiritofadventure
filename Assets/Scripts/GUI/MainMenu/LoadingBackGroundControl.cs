﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LoadingBackGroundControl : MonoBehaviour
{

    public static Action OnBackGroundHide;
    public static LoadingBackGroundControl instance;
    private Image backgound;
    [SerializeField]
    private Text backgoundText;
    [SerializeField]
    private Image backgoundImage;
    private float speedAlpha = 0.01f;
    void Start()
    {
        instance = this;
        backgound = GetComponent<Image>();
    }

    public void Hide()
    {
        StartCoroutine(Transparensy());
    }

    private IEnumerator Transparensy()
    {
        backgoundImage.enabled = false;
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (backgound.color.a < 0.05f)
            {
                backgound.color = new Color(backgound.color.r, backgound.color.g, backgound.color.b, 0);
                backgoundText.color = new Color(backgoundText.color.r, backgoundText.color.g, backgoundText.color.b, 0);
                if (OnBackGroundHide != null) { OnBackGroundHide(); }
                yield break;
            }
            else
            {
                float alpha = Mathf.Lerp(backgound.color.a,0, speedAlpha);
                //print(alpha);
                backgound.color = new Color(backgound.color.r, backgound.color.g, backgound.color.b, alpha);
                backgoundText.color = new Color(backgoundText.color.r, backgoundText.color.g, backgoundText.color.b, alpha);
            }
        }
    }
}
