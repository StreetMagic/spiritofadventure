﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollRectSnapToElement : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_Content; // To hold the ScrollPanel
    [SerializeField]
    private RectTransform m_Center;    // Center to compare the distance for each button
    [Range(1, 100)]
    [SerializeField]
    private float SpeedLerp;

    [SerializeField]
    private GameObject m_leftButton;
    [SerializeField]
    private GameObject m_rightButton;

    // Private Variables
    private float[] distance;    // All buttons' distance to the center
    private float[] distReposition;
    private RectTransform[] items;
    private bool dragging = false;  // Will be true, while we drag the panel
    private int itemsDistance;   // Will hold the distance between the buttons
    private int minItemNum;   // To hold the number of the button, with smallest distance to center
    private int itemsLength;
    private float minDistance;

    private IEnumerator myCoroutine;
    private int itemNumber = 0;

    void Start()
    {
        itemsLength = m_Content.childCount;
        items = new RectTransform[itemsLength];
        for (int i = 0; i < itemsLength; i++)
        {
            GameObject go = m_Content.transform.GetChild(i).gameObject;
            items[i] = go.GetComponent<RectTransform>();
        }
        distance = new float[itemsLength];
        distReposition = new float[itemsLength];

        // Get distance between buttons
        itemsDistance = (int)Mathf.Abs(items[1].anchoredPosition.x - items[0].anchoredPosition.x);
    }
    //void Update()
    //{
    //    for (int i = 0; i < items.Length; i++)
    //    {
    //        distReposition[i] = m_Center.position.x - items[i].position.x;
    //        if (i == 0) // Get the min distance
    //        {
    //            minDistance = distReposition[i];
    //            minItemNum = i;
    //        }
    //        else
    //        {
    //            if (Mathf.Abs(minDistance) > Mathf.Abs(distReposition[i]))
    //            {
    //                minDistance = distReposition[i];
    //                minItemNum = i;
    //            }
    //        }
    //    }

    //    if (!dragging)
    //    {
    //        LerpToItem(minDistance);
    //    }
    //}
    void LerpToItem(float minDistance)
    {
        float newX = Mathf.Lerp(m_Content.anchoredPosition.x
            , m_Content.anchoredPosition.x + minDistance
            , Time.deltaTime * SpeedLerp);

        Vector2 newPosition = new Vector2(newX, m_Content.anchoredPosition.y);

        m_Content.anchoredPosition = newPosition;
    }

    public void StartDrag()
    {
        dragging = true;
    }

    public void EndDrag()
    {
        dragging = false;
    }




    public void MoveShop(bool left)
    {
        if (myCoroutine != null)
            return;

        if (left)
            itemNumber++;
        else
            itemNumber--;
        Vector2 newPos = left ? new Vector2(m_Content.anchoredPosition.x - itemsDistance, m_Content.anchoredPosition.y) :
            new Vector2(m_Content.anchoredPosition.x + itemsDistance, m_Content.anchoredPosition.y);
        myCoroutine = MoveCoroutine(newPos);
        StartCoroutine(myCoroutine);

    }

    private IEnumerator MoveCoroutine(Vector2 endPos)
    {
        Vector2 startPos = m_Content.anchoredPosition;
        float t = 0;
        while (t < 1)
        {
            m_Content.anchoredPosition = Vector2.Lerp(startPos, endPos, t);
            t += Time.deltaTime*2f;
            yield return null;
        }

        m_leftButton.SetActive(true);
        m_rightButton.SetActive(true);
        if (itemNumber <= 0)
        {
            m_leftButton.SetActive(false);
        }
        if (itemNumber >= itemsLength - 1)
        {
            m_rightButton.SetActive(false);
        }
        myCoroutine = null;
    }
}
