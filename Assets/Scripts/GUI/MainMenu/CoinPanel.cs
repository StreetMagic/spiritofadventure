﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinPanel : MonoBehaviour
{
    [SerializeField]
    private Text m_Money;

    void Update()
    {
        UpdateTextMoney();
    }

    private void UpdateTextMoney()
    {
        int money = ProgressDataController.Instance.GetData("CoinsBank");
        m_Money.text = money.ToString();
    }
}
