﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelsMapPoint : MonoBehaviour
{
    [SerializeField]
    private Image[] Stars;

    [SerializeField]
    private Image m_LockImage;

    [SerializeField]
    private Text m_LevelNumberText;

    private Button _button;

    private Button button
    {
        get
        {
            if (_button == null)
            {
                _button = GetComponent<Button>();
            }
            return _button;
        }
    }

    public void SetPoint(UnityAction onClick, bool isUnlocked, int stars, int number)
    {
        if (!isUnlocked)
            return;

        m_LockImage.enabled = false;
        m_LevelNumberText.enabled = true;
        m_LevelNumberText.text = number.ToString();
        button.onClick.AddListener(onClick);

        if (stars > 0)
        {
            for (int i = 0; i < stars; i++)
            {
                Stars[i].enabled = true;
            }
        }

    }
}
