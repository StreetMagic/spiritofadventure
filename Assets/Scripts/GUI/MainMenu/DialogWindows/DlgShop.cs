﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DlgShop : DlgAbstract
{
    private static Action OnUpdateStats;
    [SerializeField]
    private GameObject m_Content;
    [SerializeField]
    private GameObject m_ShopItem;

    [SerializeField]
    private ScrollRectSnapToElement m_ScrollRectSnapToElement;
    [SerializeField]
    private Text m_Money;
    [SerializeField]
    private Text m_FirstBonus;
    [SerializeField]
    private Text m_SecondBonus;

    private ShopItem[] shopList;

    protected override void Awake()
    {
        OnUpdateStats += UpdateTextMoney;
        OnUpdateStats += UpdateBonusCountText;
        base.Awake();
        shopList = Resources.Load<ShopItemsData>("ObjectsDatabases/ShopItemsDatabase").ShopDatabase;
        for (int i = 0; i < shopList.Length; i++)
        {
            GameObject item = Instantiate(m_ShopItem);
            item.transform.SetParent(m_Content.transform);
            int cCount = shopList[i].Count;
            int cPrice = shopList[i].Price;
            string cName = shopList[i].Name;
            shopList[i].Action = delegate { AddBonus(cCount, cPrice, cName); };
            var go = item.GetComponent<ShopItemPanel>();
            go.FillItem(shopList[i]);
        }
        //m_Content.SetItems(shopList);
        //m_ScrollRectSnapToElement.enabled = true;
        OnUpdateStats();
    }

    private void Start()
    {
        FirebaseScreenController.Instance.ActivateScreen(ScreenTypes.shop);
    }

    private void DisableADS()
    {
        //IAP.InAppManager.Instance.BuyProductID("no_ads");
    }

    private void UpdateTextMoney()
    {
        int money = ProgressDataController.Instance.GetData("CoinsBank");
        m_Money.text = money.ToString();
    }

    private void UpdateBonusCountText()
    {
        int bonusOne = ProgressDataController.Instance.GetData(shopList[1].Name);
        m_FirstBonus.text = Mathf.Clamp(bonusOne, 0, bonusOne).ToString();
        int bonusTwo = ProgressDataController.Instance.GetData(shopList[0].Name);
        m_SecondBonus.text = Mathf.Clamp(bonusTwo, 0, bonusTwo).ToString();
    }

    public static void AddBonus(int count, int cost, string name)
    {
        int coinsBank = ProgressDataController.Instance.GetData("CoinsBank");
        if (coinsBank < cost)
            return;

        coinsBank -= cost;
        ProgressDataController.Instance.SetData("CoinsBank", coinsBank);

        int currentCount = ProgressDataController.Instance.GetData(name);
        currentCount += count;
        ProgressDataController.Instance.SetData(name, currentCount);
        OnUpdateStats();

        CustomAnalytics.Instance.SpendCoins(cost, name, count);
    }

    private void OnDestroy()
    {
        OnUpdateStats -= UpdateTextMoney;
        OnUpdateStats -= UpdateBonusCountText;
        FirebaseScreenController.Instance.DiactivateScreen();
    }
}
