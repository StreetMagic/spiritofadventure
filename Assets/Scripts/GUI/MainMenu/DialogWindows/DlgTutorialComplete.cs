﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class DlgTutorialComplete : DlgAbstract
{
    private GameUI main;

    protected new void Awake()
    {
        base.Awake();
        main = transform.root.GetComponent<GameUI>();
//        CustomAnalytics.Instance.CompleteTutorial();
    }

    private void OnEnable()
    {
        ADsManager.Instance.ViewAds();
        int nm = 1;
        Analytics.CustomEvent("LevelVictory", new Dictionary<string, object>
        {
            {"level_complete", nm }
        });
    }

    public void MainMenu()
    {
        main.LoadMainMenu();
        ProgressDataController.Instance.SetData("Tutorial", 1);
    }
}

