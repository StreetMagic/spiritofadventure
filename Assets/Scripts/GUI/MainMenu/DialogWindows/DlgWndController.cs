﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DlgWndController : MonoBehaviour
{
    public static DlgWndController instace;
    [SerializeField] private DlgPause m_dlgPause;
    [SerializeField] private DlgExit m_dlgExit;
    [SerializeField] private DlgSettings m_dlgSettings;
    [SerializeField] private DlgVictory m_dlgVictory;
    [SerializeField] private DlgDefeat m_dlgDefeat;
    [SerializeField] private DlgTutorialComplete m_dlgTutorComplete;
    [SerializeField] private DlgTutorialFailed m_dlgTutorFailed;

    public bool isOnPause = false;

    private void Awake()
    {
        if (instace != null)
            Destroy(this);
        else
            instace = this;
        DlgAbstract.DlgWasClosedEvent += DlgWasClosedEvent;
    }

    private void OnApplicationFocus(bool focus)
    {
        /* это ж сколько нервов спалит эта функция если о ней не подозревать*/
        if (!Application.isEditor)
        {
            if (SceneManager.GetActiveScene().name == "Tutorial_SampleScene")
                return;

            if (!focus)
                Pause();
        }
    }

    private void OnDestroy()
    {
        DlgAbstract.DlgWasClosedEvent -= DlgWasClosedEvent;
    }

    private void DlgWasClosedEvent(DlgAbstract obj)
    {
        Destroy(obj.gameObject);
    }

    public void Pause()
    {
        if (isOnPause)
            return;
        DlgPause dlg = Instantiate(m_dlgPause, transform);
        dlg.OpenDlg();
        isOnPause = true;

    }

    public void Exit()
    {
        DlgExit dlg = Instantiate(m_dlgExit, transform);
        dlg.OpenDlg();
    }
    public void Settings()
    {
        DlgSettings dlg = Instantiate(m_dlgSettings, transform);
        dlg.OpenDlg();
    }
    public void Victory()
    {
        DlgVictory dlg = Instantiate(m_dlgVictory, transform);
        dlg.OpenDlg();
    }
    public void Defeat()
    {
        DlgDefeat dlg = Instantiate(m_dlgDefeat, transform);
        dlg.OpenDlg();
    }
    public void Tutorial()
    {
        DlgTutorialComplete dlg = Instantiate(m_dlgTutorComplete, transform);
        dlg.OpenDlg();
    }

    public void TutorialFailed()
    {
        DlgTutorialFailed dlg = Instantiate(m_dlgTutorFailed, transform);
        dlg.OpenDlg();
    }

}
