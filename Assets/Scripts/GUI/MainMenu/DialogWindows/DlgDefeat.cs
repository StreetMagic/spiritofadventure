﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DlgDefeat : DlgAbstract
{
    [SerializeField]
    private Text m_LevelText;
    private GameUI main;

    protected new void Awake()
    {
        base.Awake();
        main = transform.root.GetComponent<GameUI>();
        m_LevelText.text = "level " + PlayerPrefs.GetString("CurrentLevel").Remove(0, 7);
    }

    public void MainMenu()
    {
        main.LoadMainMenu();
    }
    public void Replay()
    {
        main.ReloadLevel();
    }
}
