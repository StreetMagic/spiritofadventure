﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DlgLevels : DlgAbstract
{
    [SerializeField]
    private InputField m_inputField;
    [SerializeField]
    private GameObject m_ButtonsParrent;
    [SerializeField]
    private LevelButton m_ButtonPrefab;

    private void Start()
    {
        // массив данных о пройденых уровнях
        List<string> userData = new List<string>();
        // количество уровней в игре
        TextAsset[] assets = Resources.LoadAll<TextAsset>("Levels/");
        if (assets != null)
        {
            string levelsEncode = PlayerPrefs.GetString("AcessLevels");
            //дешифровка строки из настроек
            string levels = B64X.DecodeToString(levelsEncode);
            // дешифрование не вернуло нустую строку
            if (!String.IsNullOrEmpty(levels))
                userData = levels.Split(',').ToList();
            if (userData.Count < assets.Length)
            {
                var length = assets.Length;
                //заполняем недостающие уровни
                for (int i = userData.Count; i < length; i++) // i = 3 ; 3 < 10 ; i++
                {
                    userData.Add("0");
                }
            }
            //в тукущий уровень проставляем победу
            userData[0] = "1";
        }
        else
        {
            Debug.LogError("Levels not found!");
            return;
        }
        var lengthUserData = userData.Count;
        for (int i = 0; i < lengthUserData; i++)
        {
            var go = Instantiate(m_ButtonPrefab);
            go.gameObject.transform.SetParent(m_ButtonsParrent.transform);
            bool isActive = userData[i] == "1";
            //var img = (isActive) ? m_LevelImage : m_LevelLockImage;
            var text = (isActive) ? (i + 1).ToString() : "";
            var index = i + 1;
            go.Set(delegate { LoadLevel(index); }, text, isActive);
        }

        FirebaseScreenController.Instance.ActivateScreen(ScreenTypes.levelSelection);
    }

    private void OnDestroy()
    {
        FirebaseScreenController.Instance.DiactivateScreen();
    }

    public void LoadLevel()
    {
        if (Resources.Load<TextAsset>(m_inputField.text) == null)
        {
            Debug.LogError("Wrong name!");
            return;
        }
        else
        {
            PlayerPrefs.SetString("CurrentLevel", m_inputField.text);
        }
        SceneLoader.LoadLevel(new NextLevelData("SampleScene", true));
    }



    public void LoadLevel(int level)
    {
        string path = "Levels/" + level.ToString();
        if (Resources.Load<TextAsset>(path) == null)
        {
            Debug.LogError("Wrong name!");
            return;
        }
        else
        {
            PlayerPrefs.SetString("CurrentLevel", path);
        }
        SceneLoader.LoadLevel(new NextLevelData("SampleScene", true));
    }
}
