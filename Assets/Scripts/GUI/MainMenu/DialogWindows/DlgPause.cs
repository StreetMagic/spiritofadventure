﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DlgPause : DlgAbstract
{
    [SerializeField]
    protected Button m_Sound;
    [SerializeField]
    protected Button m_Music;
    [SerializeField]
    protected Sprite m_Sound_on;
    [SerializeField]
    protected Sprite m_Sound_off;
    [SerializeField]
    protected Sprite m_Music_on;
    [SerializeField]
    protected Sprite m_Music_off;
    protected GameUI main;
    protected AudioController _audio;
    protected AudioController Audio
    {
        get
        {
            if (_audio == null)
            {

                _audio = AudioController.Instance;
            }
            return _audio;
        }
    }
    protected bool OnSound = true;
    protected bool OnMusic = true; 

    protected new void Awake()
    {
        base.Awake();
        main = transform.root.GetComponent<GameUI>();
        if (main)
        {
            main.PauseGame();
        }
        m_Sound.image.sprite = (Audio.Sound) ? m_Sound_on : m_Sound_off;
        m_Music.image.sprite = (Audio.Music) ? m_Music_on : m_Music_off;
    }

    private void Start()
    {
        FirebaseScreenController.Instance.ActivateScreen(ScreenTypes.pause);
    }

    private void OnDestroy()
    {
        FirebaseScreenController.Instance.DiactivateScreen();
    }

    public void Load()
    {
        main.ResumeGame();
        main.BuildLevel();
    }
    public void Repaly()
    {
        main.ResumeGame();
        main.ReloadLevel();
    }
    public void MainMenu()
    {
        main.ResumeGame();
        main.LoadMainMenu();
    }
    public void SoundSwitch()
    {
        Audio.OnOffSound();
        m_Sound.image.sprite = (Audio.Sound) ? m_Sound_on : m_Sound_off;
        m_Music.image.sprite = (Audio.Music) ? m_Music_on : m_Music_off;
    }
    public void MusicSwitch()
    {
        Audio.OnOffMusic();
        m_Music.image.sprite = (Audio.Music) ? m_Music_on : m_Music_off;
        m_Sound.image.sprite = (Audio.Sound) ? m_Sound_on : m_Sound_off;
    }
    public override void CloseDlg()
    {
        if (main)
        {
            main.ResumeGame();
        }
        base.CloseDlg();
        if(DlgWndController.instace)
        DlgWndController.instace.isOnPause = false;
    }
}
