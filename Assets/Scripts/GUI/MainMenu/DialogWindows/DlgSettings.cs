﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DlgSettings : DlgPause
{

    private void Start()
    {
        FirebaseScreenController.Instance.ActivateScreen(ScreenTypes.settings);
    }

    private void OnDestroy()
    {
        FirebaseScreenController.Instance.DiactivateScreen();
    }
}
