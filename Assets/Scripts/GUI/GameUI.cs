﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameUI : MonoBehaviour
{
    [SerializeField]
    private Image[] stars;
    [SerializeField]
    private Color baseColor = new Color(1, 1, 1, 0.5f);
    [SerializeField]
    private InputField m_inputText;

    private void Start()
    {
        //SetStarsColor();
        //GameController.AddedStarAction += SetStarsColor;
    }

    private void OnDestroy()
    {
        ResumeGame();
        //GameController.AddedStarAction -= SetStarsColor;
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
    }

    private void SetStarsColor()
    {
        for (int i = 0; i < stars.Length; i++)
        {
            //int starsCount = GameController.LevelStars;
            //stars[i].color = i < starsCount ? CompleteColor : baseColor;
        }
    }

    public void ReloadLevel()
    {
        //int nm = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
        //UnityEngine.SceneManagement.SceneManager.LoadScene(nm);
        if (LevelRoundLoader.Instance.CoroutineBuildRoundObjects != null)
            LevelRoundLoader.Instance.StopCoroutine(LevelRoundLoader.Instance.CoroutineBuildRoundObjects);
        SceneLoader.LoadLevel(new NextLevelData("SampleScene", true));
    }

    public void LoadNextLevel()
    {
        Debug.LogWarning("LOAD NEXT LEVEL");
    }

    public void LoadMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        //SceneManager.LoadScene("MainMenu");
    }

    public void BuildLevel()
    {
        string name = m_inputText.text;
        if (string.IsNullOrEmpty(name))
            return;
        PlayerPrefs.SetString("CurrentLevel", name);
        SceneLoader.LoadLevel(new NextLevelData("SampleScene", true));
    }

}
