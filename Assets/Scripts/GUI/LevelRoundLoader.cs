﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelRoundLoader : MonoBehaviour
{
    public static LevelRoundLoader Instance { get; private set; }
    [HideInInspector]
    public int levelNumber;
    public int TotalEnemiesCount;
    public Coroutine CoroutineBuildRoundObjects = null;

    private GameObject levelBase;
    private GeneratedLevelData loadedLevelData;
    private BallsData m_ballsData;
    private PlayerCharacterData m_playerData;
    private SceneObjectsData m_objectsData;

    [HideInInspector]
    public bool isLastRound = false;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        GameStateMachine.Instance.OnSceneBuildingAction += BuildNextRound;
    }

    private void OnDestroy()
    {
        GameStateMachine.Instance.OnSceneBuildingAction -= BuildNextRound;
    }

    public void StartLevelBuild()
    {
        m_ballsData = Resources.Load<BallsData>("ObjectsDatabases/BallsDatabase");
        m_playerData = Resources.Load<PlayerCharacterData>("ObjectsDatabases/PlayerData");
        m_objectsData = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");

        isLastRound = false;
        levelNumber = 0;
        TotalEnemiesCount = 0;
        if (SceneManager.GetActiveScene().name != "Tutorial_SampleScene")
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("SampleScene"));
        string name = PlayerPrefs.GetString("CurrentLevel", "1");
        if (!string.IsNullOrEmpty(name))
        {
            TextAsset asset = Resources.Load<TextAsset>(name);
            loadedLevelData = JsonUtility.FromJson<GeneratedLevelData>(asset.ToString());

            foreach (var round in loadedLevelData.Rounds)
            {
                foreach (var enemy in round.RoundItems)
                {
                    if (enemy.Tag == "Enemy")
                    {
                        TotalEnemiesCount++;
                    }
                }
            }

            UpdateLevelData(loadedLevelData.BalanceData);
            isLastRound = levelNumber + 1 >= loadedLevelData.Rounds.Length ? true : false;
            BuildLevel(levelNumber);
            /*UI*/
            //WavePanel.Instance.RoundCount = loadedLevelData.Rounds.Length;
            //WavePanel.Instance.CurrentRound = levelNumber;
        }
    }

    public void BuildNextRound()
    {
        levelNumber++;
        if (TutorialLevel.isPlaying)
        {
            TutorialLevel.BuildNextRound();
            return;
        }
        if (levelNumber + 1 >= loadedLevelData.Rounds.Length)
        {
            isLastRound = true;
        }
        if (levelNumber >= loadedLevelData.Rounds.Length)
            return;
        else
        {
            /*UI*/
            //WavePanel.Instance.CurrentRound = levelNumber;
            StartCoroutine(BuildNextRoundCoroutine());
        }

    }

    private void BuildLevel(int nm)
    {
        levelBase = GameObject.FindGameObjectWithTag("LevelBase");
        if (levelBase == null)
        {
            levelBase = Resources.Load<GameObject>("LevelPrefabs/LevelBase");
            Instantiate(levelBase);

            for (int i = 0; i < levelBase.transform.childCount; i++)
            {
                if (levelBase.transform.GetChild(i).tag == "WalkingZone")
                {
                    SceneManagerScript.Instance.WalkingZone = levelBase.transform.GetChild(i).GetComponent<BoxCollider>();
                    break;
                }
            }
        }

        CoroutineBuildRoundObjects = StartCoroutine(BuildRoundObjectsCoroutine(nm));
    }

    public void LoadLevelByName(string name)
    {
        if (Resources.Load<TextAsset>(name) != null)
        {
            PlayerPrefs.SetString("CurrentLevel", name);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else
        {
            Debug.LogError("wrong name!");
        }
    }

    private IEnumerator BuildNextRoundCoroutine()
    {
        while (BallsManager.Instance.ActiveBalls.Count > 0)
        {
            yield return new WaitForSeconds(0.4f);
        }
        BuildLevel(levelNumber);
    }

    private IEnumerator BuildRoundObjectsCoroutine(int roundNumber)
    {
        if (roundNumber == 0)
        {
            yield return new WaitForSeconds(1.5f);
        }

        for (int i = 0; i < loadedLevelData.Rounds[roundNumber].RoundItems.Length; i++)
        {
            GeneratedLevelItem item = loadedLevelData.Rounds[roundNumber].RoundItems[i];
            GameObject obj = Resources.Load<GameObject>("LevelPrefabs/" + item.Marker);

            Vector3 pos = item.Position;
            if (roundNumber < loadedLevelData.StartWavesCount)
            {
                pos.z = 10 - (loadedLevelData.StartWavesCount - 1 - roundNumber);
            }
            else
                pos.z = 10f;

            obj = Instantiate(obj, pos, item.Rotation);

            StartObjectAtLevel starter = obj.AddComponent<StartObjectAtLevel>();
            starter.SetObjectPosition(pos);

            obj.transform.localScale = item.Scale;
            obj.name = item.Name;
            //obj.tag = item.Tag;

        }

        if (roundNumber < loadedLevelData.StartWavesCount - 1 && roundNumber < loadedLevelData.Rounds.Length)
        {
            yield return new WaitForSeconds(0.3f);
            BuildNextRound();
        }
        else
        {
            GameStateMachine.Instance.ChangeState(GameStates.PlayerTurn);
        }
    }

    private void UpdateLevelData(BalanceData data)
    {
        for (int i = 0; i < data.BallsDatabase.Length; i++)
        {
            for (int j = 0; j < m_ballsData.BallsDatabase.Length; j++)
            {
                if (data.BallsDatabase[i].Ball_Type == m_ballsData.BallsDatabase[j].Ball_Type)
                {
                    m_ballsData.BallsDatabase[i].Ball_damage = data.BallsDatabase[i].Damage;
                    m_ballsData.BallsDatabase[i].Ball_speed = data.BallsDatabase[i].Speed;
                }
            }
        }

        for (int i = 0; i < data.ObjectsDatabase.Length; i++)
        {
            for (int j = 0; j < m_objectsData.ObjectsDatabase.Length; j++)
            {
                if (data.ObjectsDatabase[i].Object_Type == m_objectsData.ObjectsDatabase[j].Object_Type)
                {
                    m_objectsData.ObjectsDatabase[j].Object_damage = data.ObjectsDatabase[i].Damage;
                    m_objectsData.ObjectsDatabase[j].Object_health = data.ObjectsDatabase[i].Health;

                    // loading skills' parameters
                    switch (data.ObjectsDatabase[i].Object_Type)
                    {
                        case ObjectType.berry:
                            m_objectsData.ObjectsDatabase[j].Object_extraBallType = data.ObjectsDatabase[i].ExtraBallType;
                            m_objectsData.ObjectsDatabase[j].Object_extraBallDuration = data.ObjectsDatabase[i].ExtraBallDuration;
                            break;
                        case ObjectType.groot:
                            m_objectsData.ObjectsDatabase[j].Object_HealthRegen = data.ObjectsDatabase[i].HealthRegenAmount;
                            break;
                        case ObjectType.stoneCube:
                            break;
                        case ObjectType.bushCube:
                            break;
                        case ObjectType.bomb:
                            m_objectsData.ObjectsDatabase[j].Object_ExplosionDamage = data.ObjectsDatabase[i].ExplosionDamage;
                            m_objectsData.ObjectsDatabase[j].Object_ExplosionRadius = data.ObjectsDatabase[i].ExplosionRadius;
                            break;
                        case ObjectType.knight:
                            m_objectsData.ObjectsDatabase[j].Object_FrontShield = data.ObjectsDatabase[i].FrontShield;
                            m_objectsData.ObjectsDatabase[j].Object_BackShield = data.ObjectsDatabase[i].BackShield;
                            m_objectsData.ObjectsDatabase[j].Object_RightShield = data.ObjectsDatabase[i].RightShield;
                            m_objectsData.ObjectsDatabase[j].Object_LeftShield = data.ObjectsDatabase[i].LeftShield;
                            break;
                        case ObjectType.ram:
                            break;
                        case ObjectType.jelly:
                            m_objectsData.ObjectsDatabase[j].Object_FragmentsCount = data.ObjectsDatabase[i].FragmentsCount;
                            break;
                        case ObjectType.gunner:
                            m_objectsData.ObjectsDatabase[j].Object_ShotDamage = data.ObjectsDatabase[i].ShotDamage;
                            m_objectsData.ObjectsDatabase[j].Object_ShootDistance = data.ObjectsDatabase[i].ShootDistance;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        m_playerData.PlayerLifes = data.PlayerLifes;
        m_playerData.BallsPerTurn = data.BallsPerTurn;
        m_playerData.BetweenBallsTimer = data.BetweenBallsTimer;
        m_playerData.CoinsPerStar = data.CoinsInChest;

        if (SceneManagerScript.Instance.OnLevelBuilt != null)
            SceneManagerScript.Instance.OnLevelBuilt();
    }
}
