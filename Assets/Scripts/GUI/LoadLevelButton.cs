﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadLevelButton : MonoBehaviour
{
    [SerializeField]
    private int levelNumber;
    [SerializeField]
    private Image[] stars;

    [SerializeField]
    private Text text;

    private Button button;
    private Coroutine loadLevelCoroutine;

    private void Start()
    {
        /*
        PlayerPrefs.SetInt("LevelStars_1", 1);
        button = GetComponent<Button>();
        int nm = levelNumber;

        int prevStars = PlayerPrefs.GetInt("LevelStars_" + (nm - 1));
        int currStars = PlayerPrefs.GetInt("LevelStars_" + nm);

        if (nm > 1 && prevStars == 0)
        {
            button.interactable = false;
        }
        else
        {
            for (int i = 0; i < stars.Length; i++)
            {
                var star = stars[i].GetComponent<Image>();
                Color color = Color.white;
                if (i < currStars)
                {
                    color = completeColor;
                }
                star.color = color;
            }
        }
        text.text = levelNumber.ToString();
*/
    }

    public void LoadLevel()
    {
        if (loadLevelCoroutine == null)
            loadLevelCoroutine = StartCoroutine(LoadLevelCoroutine());
    }

    private IEnumerator LoadLevelCoroutine()
    {
        yield return new WaitForSeconds(1f);
        UnityEngine.SceneManagement.SceneManager.LoadScene(levelNumber);
    }

    public int GetLevelNumber()
    {
        return levelNumber;
    }
}

