﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour
{
    [SerializeField]
    private Image m_splashScreen;
    [SerializeField]
    private float m_timer;

    // Use this for initialization
    private IEnumerator Start()
    {
        m_splashScreen.canvasRenderer.SetAlpha(0f);
        m_splashScreen.CrossFadeAlpha(1f, 1.5f, false);
        yield return new WaitForSeconds(2.5f);
        m_splashScreen.CrossFadeAlpha(0f, 1.5f, false);
        yield return new WaitForSeconds(2f);

        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);

        //var data = ProgressDataController.Instance.GetData("Tutorial");
        //if (data > 0)
        //{
        //    SceneManager.LoadScene("MainMenu",LoadSceneMode.Single);
        //}
        //else
        //{
        //    SceneManager.LoadScene("Tutorial_SampleScene",LoadSceneMode.Single);
        //}
    }
}
