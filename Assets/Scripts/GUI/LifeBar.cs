﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{

    public static LifeBar NewLifeBar(Vector3 position, float height = 0f)
    {
        LifeBar lifeBar = null;
        GameObject obj = Instantiate((Resources.Load("lifeBar") as GameObject));
        //position.y += height;
        obj.transform.position = position;
        lifeBar = obj.GetComponent<LifeBar>();
        //временное решение, скорей всего
        //lifeBar.GetComponentInChildren<Image>().GetComponentInChildren<RectTransform>().Rotate(60, 0, 0);

        return lifeBar;
    }

    public static LifeBar NewLifeBar(GameObject target, float height = 0f)
    {
        LifeBar lifeBar = null;
        GameObject obj = Instantiate((Resources.Load("lifeBar2") as GameObject));
        //obj.transform.position = SceneManagerScript.MainCamera.WorldToViewportPoint(target.transform.position + Vector3.up);
        obj.transform.SetParent(SceneManagerScript.Instance.m_gameCanvas.transform);
        obj.transform.localScale = Vector3.one;
        //position.y += height;
        lifeBar = obj.GetComponent<LifeBar>();
        lifeBar.target = target.GetComponent<Collider>();
        //временное решение, скорей всего
        //lifeBar.GetComponentInChildren<Image>().GetComponentInChildren<RectTransform>().Rotate(60, 0, 0);

        return lifeBar;
    }

    [SerializeField]
    private Image m_LifeBar;
    [SerializeField]
    private Slider m_Slider;
    [SerializeField]
    private Text m_Text;

    public Collider target;

    private Camera mainCam;
    private Vector2 viewportPosition;
    private Vector3 position;
    private RectTransform canvasRect;
    private RectTransform myTransform;

    //private void Start()
    //{
    //    mainCam = SceneManagerScript.MainCamera;
    //    canvasRect = SceneManagerScript.GameCanvas.GetComponent<RectTransform>();
    //    myTransform = GetComponent<RectTransform>();
    //    SetPosition();
    //    print(transform.position);
    //}

    //private void Update()
    //{
    //    SetPosition();
    //}

    //private void SetPosition()
    //{
    //    if (!target)
    //        return;
    //    position = target.bounds.center;
    //    viewportPosition = mainCam.WorldToViewportPoint(position);

    //    viewportPosition.x = viewportPosition.x * canvasRect.sizeDelta.x;
    //    viewportPosition.y = viewportPosition.y * canvasRect.sizeDelta.y;

    //    myTransform.position = viewportPosition;

    //}

    public void UpdateLifeBar(float partAmount, float fullAmount, float takenDamage)
    {
        if (takenDamage < 0) HealthUI.Instance.ShowDamage((takenDamage.ToString("#")), transform.root);
        if (takenDamage > 0) HealthUI.Instance.ShowHeal(("+" + takenDamage.ToString("#")), transform.root);
        //m_LifeBar.GetComponent<TextMesh>().text = partAmount.ToString() + "/" + fullAmount.ToString();
        //m_LifeBar.fillAmount = partAmount / fullAmount;
        m_Slider.maxValue = fullAmount;
        m_Slider.value = partAmount;
        m_Text.text = partAmount.ToString("#");
    }

    public void SetLifeBar(float partAmount, float fullAmount)
    {
        m_Slider.maxValue = fullAmount;
        m_Slider.value = partAmount;
        m_Text.text = partAmount.ToString("#");
    }

}
