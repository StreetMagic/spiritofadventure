﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelNumberText : MonoBehaviour
{
    private Text text;

    private void Start()
    {
        text = GetComponent<Text>();
        SceneManagerScript.Instance.OnLevelBuilt += ShowLevelNumber;        
    }

    private void OnDestroy()
    {
        SceneManagerScript.Instance.OnLevelBuilt -= ShowLevelNumber;
    }

    private void ShowLevelNumber()
    {
        StartCoroutine(LevelTextCoroutine());
    }

    private IEnumerator LevelTextCoroutine()
    {
        text.enabled = true;
        text.text = "Level " + PlayerPrefs.GetString("CurrentLevel").Remove(0, 7);
        Color currentColor = text.color;
        float alpha = text.color.a;
        while (alpha < 0.8f)
        {
            alpha += Time.deltaTime;
            currentColor.a = alpha;
            text.color = currentColor;
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        while (alpha > 0)
        {
            alpha -= Time.deltaTime;
            currentColor.a = alpha;
            text.color = currentColor;
            yield return null;
        }
        gameObject.SetActive(false);
    }
}
