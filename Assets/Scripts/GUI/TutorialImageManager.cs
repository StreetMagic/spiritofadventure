﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialImageManager : MonoBehaviour
{
    [SerializeField]
    private GameObject m_TutorialPanel;

    [SerializeField]
    private Image m_ActiveImage;

    [SerializeField]
    private GameObject m_FinishButton;

    [SerializeField]
    private Text m_FinishButtonText;

    private TutorialsData data;
    private Sprite[] CurrentLevelSprites;
    private bool isShown = false;
    private bool isActive = false;
    private int imageNumber;

    // Use this for initialization
    void Start()
    {
        data = Resources.Load<TutorialsData>("ObjectsDatabases/TutorialData");
        GameStateMachine.Instance.OnPlayerTurnAction += ShowTutorial;

    }

    private void OnDestroy()
    {
        GameStateMachine.Instance.OnPlayerTurnAction -= ShowTutorial;
    }

    private void ShowTutorial()
    {
        if (isShown)
            return;

        //Time.timeScale = 0;
        int number = int.Parse(PlayerPrefs.GetString("CurrentLevel").Remove(0, 7));
        if (!FindTutorial(number))
            return;

        StartCoroutine(ShowTutorCoroutine());

    }

    private bool FindTutorial(int levelNumber)
    {
        for (int i = 0; i < data.Tutorials.Length; i++)
        {
            if (data.Tutorials[i].LevelNumber == levelNumber)
            {
                CurrentLevelSprites = data.Tutorials[i].TutorialSprites;
                return true;
            }
        }
        return false;
    }

    private IEnumerator ShowTutorCoroutine()
    {
        yield return new WaitForSeconds(0.8f);

        m_ActiveImage.sprite = CurrentLevelSprites[0];
        m_TutorialPanel.SetActive(true);

        if (imageNumber >= CurrentLevelSprites.Length - 1)
        {
            m_FinishButton.SetActive(true);
            m_FinishButtonText.text = "GOT IT";
        }

        Time.timeScale = 0;
        isShown = true;
    }

    public void FinishTutor()
    {
        if (imageNumber >= CurrentLevelSprites.Length - 1)
        {
            Time.timeScale = 1;
            m_TutorialPanel.SetActive(false);
            return;
        }

        imageNumber++;
        m_ActiveImage.sprite = CurrentLevelSprites[imageNumber];

        if(imageNumber == CurrentLevelSprites.Length - 1)
        {
            m_FinishButtonText.text = "GOT IT";
        }

    }

    public void NextImage()
    {
        if (imageNumber >= CurrentLevelSprites.Length - 1)
            return;

        imageNumber++;
        m_ActiveImage.sprite = CurrentLevelSprites[imageNumber];

        if (imageNumber >= CurrentLevelSprites.Length - 1)
            m_FinishButton.SetActive(true);
    }

    public void PreviousImage()
    {
        if (imageNumber <= 0)
            return;

        imageNumber--;
        m_ActiveImage.sprite = CurrentLevelSprites[imageNumber];
    }
}
