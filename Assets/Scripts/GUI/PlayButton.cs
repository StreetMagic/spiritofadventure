﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class PlayButton : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Button button;

    [SerializeField]
    private GameObject ballChoiceButton;

    private void Reset()
    {
        if (!animator)
            animator = GetComponent<Animator>();
    }

    public void PressButtonAnimation()
    {
        animator.SetTrigger("Hide");
        button.interactable = false;
        //GameController.StopGame = false;
        ballChoiceButton.SetActive(true);
    }

    void Update()
    {
        //if (GameController.StopGame && !button.interactable)
        //{
        //    button.interactable = true;
        //    animator.SetTrigger("Unhide");
        //}
    }
}
