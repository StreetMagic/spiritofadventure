﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RescaleGridCell : MonoBehaviour
{
    [SerializeField]
    private Vector2 m_ReferenceSize;
    [SerializeField]
    private Vector2 m_ReferenceResolution;

    private float currentWidth;
    private float currentHeight;

    private GridLayoutGroup grid;
    private Vector2 cell;
    // Use this for initialization
    void Start()
    {
        grid = GetComponent<GridLayoutGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        Resize();
    }

    private void Resize()
    {
        currentWidth = Screen.width;
        currentHeight = Screen.height;
        cell = grid.cellSize;
        cell.x = m_ReferenceSize.x * currentWidth / m_ReferenceResolution.x;
        cell.y = m_ReferenceSize.y * currentHeight / m_ReferenceResolution.y;
        grid.cellSize = cell;
    }
}
