﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WavePanel : MonoBehaviour
{
    public static WavePanel Instance;
    [SerializeField]
    private Sprite m_StartImage;
    [SerializeField]
    private Sprite m_MidleImage;
    [SerializeField]
    private Sprite m_Hilight;
    [SerializeField]
    private CellWavePanel m_CellPrefab;

    private List<float> points = new List<float>();
    private List<CellWavePanel> cells = new List<CellWavePanel>();

    private static int currentRound = 0;
    public int CurrentRound
    {
        get { return currentRound; }
        set
        {
            currentRound = value;
            if (currentRound < 0)
            {
                Debug.LogError("CurrentWave < 0, CurrentWave =" + currentRound);
            }
            else
            {
                SetWave(currentRound);
            }
        }
    }
    private static int roundCount;
    public int RoundCount
    {
        get { return roundCount; }
        set { roundCount = value; SetRoundCount(roundCount); }
    }
    private void Awake()
    {
        if (Instance)
            Destroy(this);
        else
            Instance = this;
    }

    private void SetWave(int currentRound)
    {
        ////print("SetWave=" + currentRound);
        if (currentRound > cells.Count || currentRound < 0)
        {
            currentRound = 0;
        }
        int i = 0;
        for (; i <= currentRound; i++)
        {
            cells[i].Hilight = true;
        }
        for (i = currentRound + 1; i < cells.Count; i++)
        {
            cells[i].Hilight = false;
        }
    }

    private int firstRoundCount;
    public void SetRoundCount(int count)
    {
        firstRoundCount = 0;
        //print("SetRoundCount=" + count);
        if (firstRoundCount == count)
        {
            return;
        }
        else
        {
            points.Clear();
            var length = cells.Count;
            if (length > 0)
            {
                for (int i = length - 1; i >= 0; i--)
                {
                    Destroy(cells[i].gameObject, 0.1f);
                    cells.RemoveAt(i);
                }
            }
        }
        firstRoundCount = count;
        if (WavePanel.currentRound != 0)
        {

        }
        RectTransform rect = m_CellPrefab.gameObject.GetComponent<RectTransform>();
        float width = rect.sizeDelta.x;
        float currentPosition = width / 2;
        for (int i = 0; i < count; i++)
        {
            points.Add(currentPosition);
            currentPosition += width;
            var go = Instantiate(m_CellPrefab, transform);
            go.name = "cell-" + i;
            cells.Add(go);
            cells[i].transform.localPosition = new Vector3(points[i], 0, 0);
            if (currentRound != 0)
            {
                if (i <= currentRound - 1)
                {
                    cells[i].Hilight = true;
                }
            }
            if (i == 0)
            {
                cells[i].Sprite = m_StartImage;
                cells[i].current.SetNativeSize();
                //cells[i].Hilight = true;
                cells[i].transform.localScale = new Vector3(-1, 1, 1);
            }
            else if (i == count - 1)
            {
                cells[i].Sprite = m_StartImage;
                cells[i].current.SetNativeSize();
            }
            else
                cells[i].Sprite = m_MidleImage;
        }
    }


}
