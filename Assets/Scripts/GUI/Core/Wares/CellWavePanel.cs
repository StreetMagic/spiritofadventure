﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellWavePanel : MonoBehaviour
{
    [SerializeField]
    private Image m_Hilight;
    private Image _current = null;
    public Image current
    {
        get
        {
            if (!_current)
                _current = GetComponent<Image>();
            return _current;
        }
        set
        {
            if (!_current)
                _current = GetComponent<Image>();
            _current = value;
        }
    }
    public bool Hilight
    {
        get
        {
            return m_Hilight.enabled;
        }
        set
        {
            m_Hilight.enabled = value;
        }
    }
    public Sprite Sprite
    {
        get
        {
            return current.sprite;
        }
        set
        {
            current.sprite = value;
        }
    }
}
