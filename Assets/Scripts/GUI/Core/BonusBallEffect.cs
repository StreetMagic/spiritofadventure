﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusBallEffect : MonoBehaviour
{
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    private void Start()
    {
        ShowText();
    }
    public void ShowText()
    {
        anim.SetTrigger("ScrollUp");
        Destroy(gameObject, 0.8f);
    }
}
